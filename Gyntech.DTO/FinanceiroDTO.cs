﻿using System;

namespace Gyntech.DTO
{
    public class FinanceiroDTO
    {
        private int id;
        private string lancamentos;
        private string receita;
        private string despesa;
        private string valor;
        private DateTime data;
        private string dia;
        private string descricao;

        public int Id { get => id; set => id = value; }
        public string Lancamentos { get => lancamentos; set => lancamentos = value; }
        public string Receita { get => receita; set => receita = value; }
        public string Despesa { get => despesa; set => despesa = value; }
        public string Valor { get => valor; set => valor = value; }
        public DateTime Data { get => data; set => data = value; }
        public string Dia { get => dia; set => dia = value; }
        public string Descricao { get => descricao; set => descricao = value; }
    }
}
