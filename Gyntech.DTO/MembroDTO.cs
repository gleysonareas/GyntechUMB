﻿using System;

namespace Gyntech.DTO
{
    public class MembroDTO
    {
        private int id;
        private string nomeCompleto;
        private DateTime nascimento;
        private DateTime batismo;
        private string rg;
        private string cpf;
        private string nacionalidade;
        private string naturalidade;
        private string nomePai;
        private string nomeMae;
        private string estadoCivil;
        private string funcao;
        private string sexo;
        private string recebidoPor;
        private string batizado;
        private string status;
        private string telefone;
        private string celular;
        private string eMail;
        private string cep;
        private string endereco;
        private string numero;
        private string complemento;
        private string uf;
        private string cidade;
        private string bairro;

        public int Id { get => id; set => id = value; }
        public string NomeCompleto { get => nomeCompleto; set => nomeCompleto = value; }
        public DateTime Nascimento { get => nascimento; set => nascimento = value; }
        public DateTime Batismo { get => batismo; set => batismo = value; }
        public string Rg { get => rg; set => rg = value; }
        public string Cpf { get => cpf; set => cpf = value; }
        public string Nacionalidade { get => nacionalidade; set => nacionalidade = value; }
        public string Naturalidade { get => naturalidade; set => naturalidade = value; }
        public string NomePai { get => nomePai; set => nomePai = value; }
        public string NomeMae { get => nomeMae; set => nomeMae = value; }
        public string EstadoCivil { get => estadoCivil; set => estadoCivil = value; }
        public string Funcao { get => funcao; set => funcao = value; }
        public string Sexo { get => sexo; set => sexo = value; }
        public string RecebidoPor { get => recebidoPor; set => recebidoPor = value; }
        public string Batizado { get => batizado; set => batizado = value; }
        public string Status { get => status; set => status = value; }
        public string Telefone { get => telefone; set => telefone = value; }
        public string Celular { get => celular; set => celular = value; }
        public string EMail { get => eMail; set => eMail = value; }
        public string Cep { get => cep; set => cep = value; }
        public string Endereco { get => endereco; set => endereco = value; }
        public string Numero { get => numero; set => numero = value; }
        public string Complemento { get => complemento; set => complemento = value; }
        public string Uf { get => uf; set => uf = value; }
        public string Cidade { get => cidade; set => cidade = value; }
        public string Bairro { get => bairro; set => bairro = value; }
    }
}
