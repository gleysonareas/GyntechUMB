﻿using System;
using System.Windows.Forms;

namespace Gyntech.DAL
{
    public class SetupMemberBankDAL
    {
        string connectionstring = @"Server=.\SQLEXPRESS;Database=master;Trusted_Connection=True;";

        public void RestaurarDBPadraoCasoNaoExista()
        {
            try
            {
                var bancoExiste = VerificaSeBancoJaExiste();

                if (!bancoExiste)
                {
                    RestaurarDBPadrao();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public bool VerificaSeBancoJaExiste()
        {
            bool retorno = false;

            try
            {
                using (var con = new System.Data.SqlClient.SqlConnection(connectionstring))
                {
                    con.Open();
                    using (var cmd = con.CreateCommand())
                    {
                        cmd.CommandText = "SELECT 1 FROM SYS.DATABASES WHERE NAME LIKE 'member_bank'";
                        var valor = cmd.ExecuteScalar();

                        if (valor != null && valor != DBNull.Value && Convert.ToInt32(valor).Equals(1))
                        {
                            retorno = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return retorno;
        }

        public void DescobrirDiretoriosPadrao(out string diretorioDados, out string diretorioLog, out string diretorioBackup)
        {
            using (var connection = new System.Data.SqlClient.SqlConnection(connectionstring))
            {
                var serverConnection = new Microsoft.SqlServer.Management.Common.ServerConnection("connection"); //se der erro o problema é aqui!
                Microsoft.SqlServer.Management.Smo.Server server = new Microsoft.SqlServer.Management.Smo.Server(serverConnection);
                diretorioDados = !string.IsNullOrWhiteSpace(server.Settings.DefaultFile) ? server.Settings.DefaultFile : (!string.IsNullOrWhiteSpace(server.DefaultFile) ? server.DefaultFile : server.MasterDBPath);
                diretorioLog = !string.IsNullOrWhiteSpace(server.Settings.DefaultLog) ? server.Settings.DefaultLog : (!string.IsNullOrWhiteSpace(server.DefaultLog) ? server.DefaultLog : server.MasterDBLogPath);
                diretorioBackup = !string.IsNullOrWhiteSpace(server.Settings.BackupDirectory) ? server.Settings.BackupDirectory : server.BackupDirectory;
            }
        }

        public void RestaurarDBPadrao()
        {
            try
            {
                string diretorioDados, diretorioLog, diretorioBackup;
                DescobrirDiretoriosPadrao(out diretorioDados, out diretorioLog, out diretorioBackup);

                using (var conn = new System.Data.SqlClient.SqlConnection(connectionstring))
                {
                    conn.Open();
                    using (var comm = conn.CreateCommand())
                    {
                        var caminhoCompletoBackup = System.IO.Path.Combine(diretorioBackup, "member_bank.bak");
                        var caminhoCompletoDados = System.IO.Path.Combine(diretorioDados, "member_bank.mdf");
                        var caminhoCompletoLog = System.IO.Path.Combine(diretorioLog, "member_bank_Log.ldf");
                        System.IO.File.Copy("member_bank.bak", caminhoCompletoBackup, true);
                        comm.CommandText =
                            @"RESTORE DATABASE member_bank " +
                            @"FROM DISK = N'" + caminhoCompletoBackup + "' " +
                            @"WITH FILE = 1, " +
                            @"MOVE N'member_bank' TO N'" + caminhoCompletoDados + "', " +
                            @"MOVE N'member_bank_LOG' TO N'" + caminhoCompletoLog + "', " +
                            @"NOUNLOAD, REPLACE, STATS = 10";
                        comm.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
