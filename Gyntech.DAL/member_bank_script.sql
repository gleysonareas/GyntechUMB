USE [master]
GO
/****** Object:  Database [member_bank]    Script Date: 16/02/2019 12:54:35 ******/
CREATE DATABASE [member_bank]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'member_bank', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\member_bank.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'member_bank_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\member_bank_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [member_bank] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [member_bank].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [member_bank] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [member_bank] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [member_bank] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [member_bank] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [member_bank] SET ARITHABORT OFF 
GO
ALTER DATABASE [member_bank] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [member_bank] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [member_bank] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [member_bank] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [member_bank] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [member_bank] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [member_bank] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [member_bank] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [member_bank] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [member_bank] SET  DISABLE_BROKER 
GO
ALTER DATABASE [member_bank] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [member_bank] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [member_bank] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [member_bank] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [member_bank] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [member_bank] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [member_bank] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [member_bank] SET RECOVERY FULL 
GO
ALTER DATABASE [member_bank] SET  MULTI_USER 
GO
ALTER DATABASE [member_bank] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [member_bank] SET DB_CHAINING OFF 
GO
ALTER DATABASE [member_bank] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [member_bank] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [member_bank] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [member_bank] SET QUERY_STORE = OFF
GO
USE [member_bank]
GO
ALTER DATABASE SCOPED CONFIGURATION SET IDENTITY_CACHE = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [member_bank]
GO
/****** Object:  Table [dbo].[acaoSocial]    Script Date: 16/02/2019 12:54:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[acaoSocial](
	[id] [int] NOT NULL,
	[lancamento] [nvarchar](50) NOT NULL,
	[entrada] [nvarchar](50) NULL,
	[saida] [nvarchar](50) NULL,
	[nomeAlimento] [nvarchar](100) NOT NULL,
	[quantidade] [nvarchar](50) NULL,
	[fabricante] [nvarchar](50) NULL,
	[validade] [date] NOT NULL,
	[dataEntrada] [date] NOT NULL,
	[dataSaida] [date] NULL,
	[descricao] [text] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[login]    Script Date: 16/02/2019 12:54:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[login](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nome] [nvarchar](300) NOT NULL,
	[login] [nvarchar](50) NOT NULL,
	[senha] [nvarchar](50) NOT NULL,
	[confirmarSenha] [nvarchar](50) NOT NULL,
	[função] [nvarchar](50) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[membros]    Script Date: 16/02/2019 12:54:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[membros](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nomeCompleto] [nvarchar](300) NOT NULL,
	[dataNascimento] [date] NOT NULL,
	[dataBatismo] [date] NOT NULL,
	[rg] [nvarchar](20) NOT NULL,
	[cpf] [nvarchar](20) NOT NULL,
	[nacionalidade] [nvarchar](100) NOT NULL,
	[naturalidade] [nvarchar](100) NOT NULL,
	[estadoCivil] [nvarchar](100) NOT NULL,
	[funcao] [nvarchar](100) NOT NULL,
	[nomePai] [nvarchar](300) NULL,
	[nomeMae] [nvarchar](300) NULL,
	[sexo] [nvarchar](15) NOT NULL,
	[recebidoPor] [nvarchar](30) NOT NULL,
	[batizado] [nvarchar](10) NOT NULL,
	[status] [nvarchar](10) NOT NULL,
	[telefone] [nvarchar](20) NULL,
	[celular] [nvarchar](20) NOT NULL,
	[eMail] [nvarchar](300) NULL,
	[cep] [nvarchar](20) NOT NULL,
	[endereco] [text] NOT NULL,
	[numero] [nvarchar](20) NOT NULL,
	[complemento] [text] NULL,
	[uf] [nvarchar](20) NOT NULL,
	[cidade] [nvarchar](300) NOT NULL,
	[bairro] [nvarchar](300) NOT NULL,
	[imagens] [text] NULL,
 CONSTRAINT [PK_tabelaTeste] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[receitasEdespesas]    Script Date: 16/02/2019 12:54:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[receitasEdespesas](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[lancamentos] [nvarchar](50) NOT NULL,
	[receita] [nvarchar](50) NULL,
	[despesa] [nvarchar](50) NULL,
	[valor] [nvarchar](50) NOT NULL,
	[data] [date] NOT NULL,
	[dia] [nvarchar](50) NOT NULL,
	[descricao] [text] NOT NULL,
 CONSTRAINT [PK_receitasEdespesas] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
USE [master]
GO
ALTER DATABASE [member_bank] SET  READ_WRITE 
GO
