﻿using Gyntech.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Gyntech.DAL
{
    public class MembroDAL
    {
        string connectionstring = @"Data Source=.\SQLEXPRESS;Initial Catalog=member_bank;Integrated Security=True;Connect Timeout=30";

        #region Salvar no banco
        //Função que realiza o INSERT
        public int Inserir(MembroDTO mDto)
        {
            using (SqlConnection con = new SqlConnection(connectionstring))
            {
                SqlCommand cmd = new SqlCommand();

                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "INSERT INTO membros ([nomeCompleto], [dataNascimento], [dataBatismo], [rg], [cpf], [nacionalidade], [naturalidade], [estadoCivil]," +
                    "[funcao], [nomePai], [nomeMae], [sexo], [recebidoPor], [batizado], [status], [telefone], [celular], [eMail], [cep], [endereco], [numero], [complemento], [uf]," +
                    "[cidade], [bairro]) VALUES (@nomeCompleto, @dataNascimento, @dataBatismo, @rg, @cpf, @nacionalidade, @naturalidade, @estadoCivil," +
                    "@funcao, @nomePai, @nomeMae, @sexo, @recebidoPor, @batizado, @status, @telefone, @celular, @eMail, @cep, @endereco, @numero, @complemento, @uf, " +
                    "@cidade, @bairro)";

                cmd.Parameters.Add("nomeCompleto", SqlDbType.NVarChar).Value = mDto.NomeCompleto;
                cmd.Parameters.Add("dataNascimento", SqlDbType.Date).Value = mDto.Nascimento;
                cmd.Parameters.Add("dataBatismo", SqlDbType.Date).Value = mDto.Batismo;
                cmd.Parameters.Add("rg", SqlDbType.NVarChar).Value = mDto.Rg;
                cmd.Parameters.Add("cpf", SqlDbType.NVarChar).Value = mDto.Cpf;
                cmd.Parameters.Add("nacionalidade", SqlDbType.NVarChar).Value = mDto.Nacionalidade;
                cmd.Parameters.Add("naturalidade", SqlDbType.NVarChar).Value = mDto.Naturalidade;
                cmd.Parameters.Add("estadoCivil", SqlDbType.NVarChar).Value = mDto.EstadoCivil;
                cmd.Parameters.Add("funcao", SqlDbType.NVarChar).Value = mDto.Funcao;

                if (mDto.NomePai != "")
                    cmd.Parameters.Add("nomePai", SqlDbType.NVarChar).Value = mDto.NomePai;
                else
                    cmd.Parameters.AddWithValue("nomePai", DBNull.Value);

                if (mDto.NomeMae != "")
                    cmd.Parameters.Add("nomeMae", SqlDbType.NVarChar).Value = mDto.NomeMae;
                else
                    cmd.Parameters.AddWithValue("nomeMae", DBNull.Value);

                cmd.Parameters.Add("sexo", SqlDbType.NVarChar).Value = mDto.Sexo;
                cmd.Parameters.Add("recebidoPor", SqlDbType.NVarChar).Value = mDto.RecebidoPor;
                cmd.Parameters.Add("batizado", SqlDbType.NVarChar).Value = mDto.Batizado;
                cmd.Parameters.Add("status", SqlDbType.NVarChar).Value = mDto.Status;

                if (mDto.Telefone != "")
                    cmd.Parameters.Add("telefone", SqlDbType.NVarChar).Value = mDto.Telefone;
                else
                    cmd.Parameters.AddWithValue("telefone", DBNull.Value);

                cmd.Parameters.Add("celular", SqlDbType.NVarChar).Value = mDto.Celular;

                if (mDto.EMail != "")
                    cmd.Parameters.Add("eMail", SqlDbType.NVarChar).Value = mDto.EMail;
                else
                    cmd.Parameters.AddWithValue("eMail", DBNull.Value);

                cmd.Parameters.Add("cep", SqlDbType.NVarChar).Value = mDto.Cep;
                cmd.Parameters.Add("endereco", SqlDbType.Text).Value = mDto.Endereco;
                cmd.Parameters.Add("numero", SqlDbType.NVarChar).Value = mDto.Numero;

                if (mDto.Complemento != "")
                    cmd.Parameters.Add("complemento", SqlDbType.Text).Value = mDto.Complemento;
                else
                    cmd.Parameters.AddWithValue("complemento", DBNull.Value);

                cmd.Parameters.Add("uf", SqlDbType.NVarChar).Value = mDto.Uf;
                cmd.Parameters.Add("cidade", SqlDbType.NVarChar).Value = mDto.Cidade;
                cmd.Parameters.Add("bairro", SqlDbType.NVarChar).Value = mDto.Bairro;

                cmd.Connection = con;
                con.Open();
                int qtd = cmd.ExecuteNonQuery();
                return qtd;
            }

        }

        #endregion

        #region Carregar Membros
        //Função que realiza o SELECT
        public IList<MembroDTO> CarregarMembros()
        {
            using (SqlConnection con = new SqlConnection(connectionstring))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "select * from membros";
                cmd.Connection = con;
                SqlDataReader dr;
                IList<MembroDTO> listaMembrosDTO = new List<MembroDTO>();
                con.Open();
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        MembroDTO mDTO = new MembroDTO();
                        mDTO.Id = Convert.ToInt32(dr["id"]);
                        mDTO.NomeCompleto = Convert.ToString(dr["nomeCompleto"]);
                        mDTO.EstadoCivil = Convert.ToString(dr["estadoCivil"]);
                        mDTO.Funcao = Convert.ToString(dr["funcao"]);
                        mDTO.Sexo = Convert.ToString(dr["sexo"]);
                        mDTO.Celular = Convert.ToString(dr["celular"]);
                        mDTO.Endereco = Convert.ToString(dr["endereco"]);
                        mDTO.Bairro = Convert.ToString(dr["bairro"]);
                        mDTO.Status = Convert.ToString(dr["status"]);

                        listaMembrosDTO.Add(mDTO);
                    }
                }
                return listaMembrosDTO;
            }
        }
        #endregion

        #region Carregar Do Banco
        //Select
        public IList<MembroDTO> CarregarDoBanco(int idMembroSelecionado)
        {
            using (SqlConnection con = new SqlConnection(connectionstring))
            {
                MembroDTO mDTO = new MembroDTO();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "select * from membros WHERE id=" + idMembroSelecionado;
                cmd.Connection = con;
                SqlDataReader dr;
                IList<MembroDTO> membroSelecionadoDTO = new List<MembroDTO>();
                con.Open();
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        mDTO.Id = Convert.ToInt32(dr["id"]);
                        mDTO.NomeCompleto = Convert.ToString(dr["nomeCompleto"]);
                        mDTO.Nascimento = Convert.ToDateTime(dr["dataNascimento"]);
                        mDTO.Batismo = Convert.ToDateTime(dr["dataBatismo"]);
                        mDTO.Rg = Convert.ToString(dr["rg"]);
                        mDTO.Cpf = Convert.ToString(dr["cpf"]);
                        mDTO.Nacionalidade = Convert.ToString(dr["nacionalidade"]);
                        mDTO.Naturalidade = Convert.ToString(dr["naturalidade"]);
                        mDTO.NomePai = Convert.ToString(dr["nomePai"]);
                        mDTO.NomeMae = Convert.ToString(dr["nomeMae"]);
                        mDTO.EstadoCivil = Convert.ToString(dr["estadoCivil"]);
                        mDTO.Funcao = Convert.ToString(dr["funcao"]);
                        mDTO.Sexo = Convert.ToString(dr["sexo"]);
                        mDTO.RecebidoPor = Convert.ToString(dr["recebidoPor"]);
                        mDTO.Batizado = Convert.ToString(dr["batizado"]);
                        mDTO.Status = Convert.ToString(dr["status"]);
                        mDTO.Telefone = Convert.ToString(dr["telefone"]);
                        mDTO.Celular = Convert.ToString(dr["celular"]);
                        mDTO.EMail = Convert.ToString(dr["email"]);
                        mDTO.Cep = Convert.ToString(dr["cep"]);
                        mDTO.Endereco = Convert.ToString(dr["endereco"]);
                        mDTO.Numero = Convert.ToString(dr["numero"]);
                        mDTO.Complemento = Convert.ToString(dr["complemento"]);
                        mDTO.Uf = Convert.ToString(dr["uf"]);
                        mDTO.Cidade = Convert.ToString(dr["cidade"]);
                        mDTO.Bairro = Convert.ToString(dr["bairro"]);


                        membroSelecionadoDTO.Add(mDTO);
                    }
                }
                return membroSelecionadoDTO;
            }
        }
        #endregion

        #region Atulizar no banco
        //Função que realiza UPDATE no banco de dados
        public int Atulizar(MembroDTO mDto)
        {
            using (SqlConnection con = new SqlConnection(connectionstring))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @"UPDATE membros SET
                [nomeCompleto] = @nomeCompleto, 
                [dataNascimento] = @dataNascimento, 
                [dataBatismo] = @dataBatismo, 
                [rg]= @rg, 
                [cpf] = @cpf, 
                [nacionalidade] = @nacionalidade, 
                [naturalidade] = @naturalidade, 
                [estadoCivil] = @estadoCivil, 
                [funcao] = @funcao, 
                [nomePai] = @nomePai, 
                [nomeMae] = @nomeMae, 
                [sexo] = @sexo, 
                [recebidoPor] = @recebidoPor, 
                [batizado] = @batizado, 
                [status] = @status,
                [telefone]= @telefone, 
                [celular] = @celular, 
                [eMail] = @eMail, 
                [cep] = @cep, 
                [endereco] = @endereco, 
                [numero] = @numero, 
                [complemento] = @complemento, 
                [uf] = @uf, 
                [cidade] = @cidade, 
                [bairro] = @bairro  
                WHERE id = @id";

                cmd.Parameters.Add("@nomeCompleto", SqlDbType.NVarChar).Value = mDto.NomeCompleto;
                cmd.Parameters.Add("@dataNascimento", SqlDbType.Date).Value = mDto.Nascimento;
                cmd.Parameters.Add("@dataBatismo", SqlDbType.Date).Value = mDto.Batismo;
                cmd.Parameters.Add("@rg", SqlDbType.NVarChar).Value = mDto.Rg;
                cmd.Parameters.Add("@cpf", SqlDbType.NVarChar).Value = mDto.Cpf;
                cmd.Parameters.Add("@nacionalidade", SqlDbType.NVarChar).Value = mDto.Nacionalidade;
                cmd.Parameters.Add("@naturalidade", SqlDbType.NVarChar).Value = mDto.Naturalidade;
                cmd.Parameters.Add("@estadoCivil", SqlDbType.NVarChar).Value = mDto.EstadoCivil;
                cmd.Parameters.Add("@funcao", SqlDbType.NVarChar).Value = mDto.Funcao;

                if (mDto.NomePai != "")
                    cmd.Parameters.Add("@nomePai", SqlDbType.NVarChar).Value = mDto.NomePai;
                else
                    cmd.Parameters.AddWithValue("@nomePai", DBNull.Value);

                if (mDto.NomeMae != "")
                    cmd.Parameters.Add("@nomeMae", SqlDbType.NVarChar).Value = mDto.NomeMae;
                else
                    cmd.Parameters.AddWithValue("@nomeMae", DBNull.Value);

                cmd.Parameters.Add("@sexo", SqlDbType.NVarChar).Value = mDto.Sexo;
                cmd.Parameters.Add("@recebidoPor", SqlDbType.NVarChar).Value = mDto.RecebidoPor;
                cmd.Parameters.Add("@batizado", SqlDbType.NVarChar).Value = mDto.Batizado;
                cmd.Parameters.Add("@status", SqlDbType.NVarChar).Value = mDto.Status;

                if (mDto.Telefone != "")
                    cmd.Parameters.Add("@telefone", SqlDbType.NVarChar).Value = mDto.Telefone;
                else
                    cmd.Parameters.AddWithValue("@telefone", DBNull.Value);

                cmd.Parameters.Add("@celular", SqlDbType.NVarChar).Value = mDto.Celular;

                if (mDto.EMail != "")
                    cmd.Parameters.Add("@eMail", SqlDbType.NVarChar).Value = mDto.EMail;
                else
                    cmd.Parameters.AddWithValue("@eMail", DBNull.Value);

                cmd.Parameters.Add("@cep", SqlDbType.NVarChar).Value = mDto.Cep;
                cmd.Parameters.Add("@endereco", SqlDbType.Text).Value = mDto.Endereco;
                cmd.Parameters.Add("@numero", SqlDbType.NVarChar).Value = mDto.Numero;

                if (mDto.Complemento != "")
                    cmd.Parameters.Add("@complemento", SqlDbType.Text).Value = mDto.Complemento;
                else
                    cmd.Parameters.AddWithValue("@complemento", DBNull.Value);

                cmd.Parameters.Add("@uf", SqlDbType.NVarChar).Value = mDto.Uf;
                cmd.Parameters.Add("@cidade", SqlDbType.NVarChar).Value = mDto.Cidade;
                cmd.Parameters.Add("@bairro", SqlDbType.NVarChar).Value = mDto.Bairro;
                cmd.Parameters.Add("@id", SqlDbType.Int).Value = mDto.Id;

                cmd.Connection = con;
                con.Open();
                int qtd = cmd.ExecuteNonQuery();
                return qtd;
            }
        }
        #endregion

        #region Alterar Status
        //Alterar Status
        public int AlterarStatus(MembroDTO mDto)
        {
            using (SqlConnection con = new SqlConnection(connectionstring))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @"UPDATE membros SET
                    [status] = @status
                    WHERE id = @id";

                cmd.Parameters.Add("@status", SqlDbType.NVarChar).Value = mDto.Status;
                cmd.Parameters.Add("@id", SqlDbType.Int).Value = mDto.Id;

                cmd.Connection = con;

                con.Open();
                int qtd = cmd.ExecuteNonQuery();
                return qtd;
            }
        }
        #endregion
    }
}