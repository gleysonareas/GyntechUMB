﻿using System.Data;
using System.Data.SqlClient;
using Gyntech.DTO;
using System.Collections.Generic;
using System;

namespace Gyntech.DAL
{
   public class FinanceiroDAL
    {
        string connectionstring = @"Data Source=.\SQLEXPRESS;Initial Catalog=member_bank;Integrated Security=True;Connect Timeout=30";

        #region salvar no banco
        //Insert
        public int Inserir(FinanceiroDTO fDTO)
        {
            using (SqlConnection con = new SqlConnection(connectionstring))
            {
                SqlCommand cmd = new SqlCommand();

                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "INSERT INTO receitasEdespesas ([lancamentos], [receita], [despesa], [valor], [data], [dia], [descricao])" +
                    "VALUES (@lancamentos, @receita, @despesa, @valor, @data, @dia, @descricao)";

                cmd.Parameters.Add("lancamentos", SqlDbType.NVarChar).Value = fDTO.Lancamentos;
                if (fDTO.Receita != "")
                    cmd.Parameters.Add("receita", SqlDbType.NVarChar).Value = fDTO.Receita;
                else
                    cmd.Parameters.AddWithValue("receita", DBNull.Value);
                if (fDTO.Despesa != "")
                    cmd.Parameters.Add("despesa", SqlDbType.NVarChar).Value = fDTO.Despesa;
                else
                    cmd.Parameters.AddWithValue("despesa", DBNull.Value);
                cmd.Parameters.Add("valor", SqlDbType.NVarChar).Value = fDTO.Valor;
                cmd.Parameters.Add("data", SqlDbType.Date).Value = fDTO.Data;
                cmd.Parameters.Add("dia", SqlDbType.NVarChar).Value = fDTO.Dia;
                cmd.Parameters.Add("descricao", SqlDbType.Text).Value = fDTO.Descricao;

                cmd.Connection = con;
                con.Open();
                int qtd = cmd.ExecuteNonQuery();
                return qtd;
            }
        }
        #endregion

        #region Atualizar
        //Update
        public int Atualizar(FinanceiroDTO fDTO)
        {
            using (SqlConnection con = new SqlConnection(connectionstring))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @"UPDATE receitasEdespesas SET
                [lancamentos] = @lancamentos,
                [receita] = @receita,
                [despesa] = @despesa,
                [valor] = @valor,
                [data] = @data,
                [dia] = @dia,
                [descricao] = @descricao
                WHERE id = @id";

                cmd.Parameters.Add("lancamentos", SqlDbType.NVarChar).Value = fDTO.Lancamentos;
                if (fDTO.Receita != "")
                    cmd.Parameters.Add("receita", SqlDbType.NVarChar).Value = fDTO.Receita;
                else
                    cmd.Parameters.AddWithValue("receita", DBNull.Value);
                if (fDTO.Despesa != "")
                    cmd.Parameters.Add("despesa", SqlDbType.NVarChar).Value = fDTO.Despesa;
                else
                    cmd.Parameters.AddWithValue("despesa", DBNull.Value);
                cmd.Parameters.Add("valor", SqlDbType.NVarChar).Value = fDTO.Valor;
                cmd.Parameters.Add("data", SqlDbType.Date).Value = fDTO.Data;
                cmd.Parameters.Add("dia", SqlDbType.NVarChar).Value = fDTO.Dia;
                cmd.Parameters.Add("descricao", SqlDbType.Text).Value = fDTO.Descricao;
                cmd.Parameters.Add("@id", SqlDbType.Int).Value = fDTO.Id;

                cmd.Connection = con;
                con.Open();
                int qtd = cmd.ExecuteNonQuery();
                return qtd;
            }
        }
        #endregion

        #region Carregar Finanças
        //Select sem parametro
        public IList<FinanceiroDTO> CarregarFinancas()
        {
            using (SqlConnection con = new SqlConnection(connectionstring))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT * FROM receitasEdespesas";
                cmd.Connection = con;
                SqlDataReader dr;
                IList<FinanceiroDTO> listaFinanceiraDTO = new List<FinanceiroDTO>();
                con.Open();
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        FinanceiroDTO fDTO = new FinanceiroDTO();
                        fDTO.Id = Convert.ToInt32(dr["id"]);
                        fDTO.Lancamentos = Convert.ToString(dr["lancamentos"]);
                        fDTO.Receita = Convert.ToString(dr["receita"]); ;
                        fDTO.Despesa = Convert.ToString(dr["despesa"]); ;
                        fDTO.Valor = Convert.ToString(dr["valor"]); ;
                        fDTO.Data = Convert.ToDateTime(dr["data"]);
                        fDTO.Dia = Convert.ToString(dr["dia"]);
                        fDTO.Descricao = Convert.ToString(dr["descricao"]);

                        listaFinanceiraDTO.Add(fDTO);
                    }
                }
                return listaFinanceiraDTO;
            }
        }
        #endregion

        #region Carregar do banco
        //Select com parametro
        public IList<FinanceiroDTO> CarregarDoBanco(int idSelecionado)
        {
            using (SqlConnection con = new SqlConnection(connectionstring))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT * FROM receitasEdespesas WHERE id=" + idSelecionado;
                cmd.Connection = con;
                SqlDataReader dr;
                IList<FinanceiroDTO> listaFinanceiraDTO = new List<FinanceiroDTO>();
                con.Open();
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        FinanceiroDTO fDTO = new FinanceiroDTO();
                        fDTO.Id = Convert.ToInt32(dr["id"]);
                        fDTO.Lancamentos = Convert.ToString(dr["lancamentos"]);
                        fDTO.Receita = Convert.ToString(dr["receita"]); ;
                        fDTO.Despesa = Convert.ToString(dr["despesa"]); ;
                        fDTO.Valor = Convert.ToString(dr["valor"]); ;
                        fDTO.Data = Convert.ToDateTime(dr["data"]);
                        fDTO.Dia = Convert.ToString(dr["dia"]);
                        fDTO.Descricao = Convert.ToString(dr["descricao"]);

                        listaFinanceiraDTO.Add(fDTO);
                    }
                }
                return listaFinanceiraDTO;
            }
        }
        #endregion
    }
}
