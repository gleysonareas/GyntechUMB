﻿namespace GyntechUMB
{
    partial class frmListaMembro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmListaMembro));
            this.pnlBaseForm = new System.Windows.Forms.Panel();
            this.lblReligar = new System.Windows.Forms.Label();
            this.lblDesligar = new System.Windows.Forms.Label();
            this.lblEditar = new System.Windows.Forms.Label();
            this.btnImprimir = new System.Windows.Forms.Button();
            this.lblBemVindo = new System.Windows.Forms.Label();
            this.btnNovo = new System.Windows.Forms.Button();
            this.gdMembros = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nomeCompleto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.funcao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.estadoCivil = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.celular = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.endereco = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bairro = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sexo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlBaseForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gdMembros)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlBaseForm
            // 
            this.pnlBaseForm.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.pnlBaseForm.Controls.Add(this.lblReligar);
            this.pnlBaseForm.Controls.Add(this.lblDesligar);
            this.pnlBaseForm.Controls.Add(this.lblEditar);
            this.pnlBaseForm.Controls.Add(this.btnImprimir);
            this.pnlBaseForm.Controls.Add(this.lblBemVindo);
            this.pnlBaseForm.Controls.Add(this.btnNovo);
            this.pnlBaseForm.Controls.Add(this.gdMembros);
            this.pnlBaseForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlBaseForm.Location = new System.Drawing.Point(0, 0);
            this.pnlBaseForm.Name = "pnlBaseForm";
            this.pnlBaseForm.Size = new System.Drawing.Size(758, 501);
            this.pnlBaseForm.TabIndex = 64;
            // 
            // lblReligar
            // 
            this.lblReligar.AutoSize = true;
            this.lblReligar.Location = new System.Drawing.Point(527, 476);
            this.lblReligar.Name = "lblReligar";
            this.lblReligar.Size = new System.Drawing.Size(220, 13);
            this.lblReligar.TabIndex = 8;
            this.lblReligar.Text = "Pressione SPACE para reintegrar um Membro";
            // 
            // lblDesligar
            // 
            this.lblDesligar.AutoSize = true;
            this.lblDesligar.Location = new System.Drawing.Point(527, 457);
            this.lblDesligar.Name = "lblDesligar";
            this.lblDesligar.Size = new System.Drawing.Size(219, 13);
            this.lblDesligar.TabIndex = 7;
            this.lblDesligar.Text = "Pressione DELETE para desligar um Membro";
            // 
            // lblEditar
            // 
            this.lblEditar.AutoSize = true;
            this.lblEditar.Location = new System.Drawing.Point(527, 438);
            this.lblEditar.Name = "lblEditar";
            this.lblEditar.Size = new System.Drawing.Size(184, 13);
            this.lblEditar.TabIndex = 6;
            this.lblEditar.Text = "Pressione F5 para Editar um Cadastro";
            // 
            // btnImprimir
            // 
            this.btnImprimir.Location = new System.Drawing.Point(3, 466);
            this.btnImprimir.Name = "btnImprimir";
            this.btnImprimir.Size = new System.Drawing.Size(137, 32);
            this.btnImprimir.TabIndex = 3;
            this.btnImprimir.Text = "Imprimir / F11";
            this.btnImprimir.UseVisualStyleBackColor = true;
            this.btnImprimir.Click += new System.EventHandler(this.btnImprimir_Click);
            // 
            // lblBemVindo
            // 
            this.lblBemVindo.AutoSize = true;
            this.lblBemVindo.Font = new System.Drawing.Font("Segoe Print", 25F, System.Drawing.FontStyle.Italic);
            this.lblBemVindo.ForeColor = System.Drawing.Color.DarkCyan;
            this.lblBemVindo.Location = new System.Drawing.Point(222, 9);
            this.lblBemVindo.Name = "lblBemVindo";
            this.lblBemVindo.Size = new System.Drawing.Size(329, 59);
            this.lblBemVindo.TabIndex = 0;
            this.lblBemVindo.Text = "Lista de Membros";
            this.lblBemVindo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnNovo
            // 
            this.btnNovo.Location = new System.Drawing.Point(3, 428);
            this.btnNovo.Name = "btnNovo";
            this.btnNovo.Size = new System.Drawing.Size(137, 32);
            this.btnNovo.TabIndex = 2;
            this.btnNovo.Text = "Novo / F9";
            this.btnNovo.UseVisualStyleBackColor = true;
            this.btnNovo.Click += new System.EventHandler(this.btnNovo_Click);
            // 
            // gdMembros
            // 
            this.gdMembros.AllowUserToAddRows = false;
            this.gdMembros.AllowUserToDeleteRows = false;
            this.gdMembros.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gdMembros.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.gdMembros.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gdMembros.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.status,
            this.nomeCompleto,
            this.funcao,
            this.estadoCivil,
            this.celular,
            this.endereco,
            this.bairro,
            this.sexo});
            this.gdMembros.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.gdMembros.Location = new System.Drawing.Point(3, 71);
            this.gdMembros.MultiSelect = false;
            this.gdMembros.Name = "gdMembros";
            this.gdMembros.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gdMembros.Size = new System.Drawing.Size(752, 351);
            this.gdMembros.TabIndex = 1;
            this.gdMembros.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gdMembros_CellDoubleClick);
            this.gdMembros.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gdMembros_KeyDown);
            // 
            // id
            // 
            this.id.DataPropertyName = "Id";
            this.id.HeaderText = "Id";
            this.id.Name = "id";
            this.id.Width = 40;
            // 
            // status
            // 
            this.status.DataPropertyName = "Status";
            this.status.HeaderText = "Status";
            this.status.Name = "status";
            this.status.Width = 60;
            // 
            // nomeCompleto
            // 
            this.nomeCompleto.DataPropertyName = "NomeCompleto";
            this.nomeCompleto.HeaderText = "Nome";
            this.nomeCompleto.Name = "nomeCompleto";
            // 
            // funcao
            // 
            this.funcao.DataPropertyName = "Funcao";
            this.funcao.HeaderText = "Função";
            this.funcao.Name = "funcao";
            this.funcao.Width = 60;
            // 
            // estadoCivil
            // 
            this.estadoCivil.DataPropertyName = "EstadoCivil";
            this.estadoCivil.HeaderText = "Estado Civil";
            this.estadoCivil.Name = "estadoCivil";
            this.estadoCivil.Width = 85;
            // 
            // celular
            // 
            this.celular.DataPropertyName = "Celular";
            this.celular.HeaderText = "Celular";
            this.celular.Name = "celular";
            // 
            // endereco
            // 
            this.endereco.DataPropertyName = "Endereco";
            this.endereco.HeaderText = "Endereço";
            this.endereco.Name = "endereco";
            // 
            // bairro
            // 
            this.bairro.DataPropertyName = "Bairro";
            this.bairro.HeaderText = "Bairro";
            this.bairro.Name = "bairro";
            // 
            // sexo
            // 
            this.sexo.DataPropertyName = "Sexo";
            this.sexo.HeaderText = "Sexo";
            this.sexo.Name = "sexo";
            this.sexo.Width = 60;
            // 
            // frmListaMembro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(758, 501);
            this.Controls.Add(this.pnlBaseForm);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(300, 141);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmListaMembro";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Lista de Membro";
            this.Activated += new System.EventHandler(this.frmMemberBank_Activated);
            this.pnlBaseForm.ResumeLayout(false);
            this.pnlBaseForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gdMembros)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlBaseForm;
        private System.Windows.Forms.Label lblDesligar;
        private System.Windows.Forms.Label lblEditar;
        private System.Windows.Forms.Button btnImprimir;
        private System.Windows.Forms.Label lblBemVindo;
        private System.Windows.Forms.Button btnNovo;
        private System.Windows.Forms.DataGridView gdMembros;
        private System.Windows.Forms.Label lblReligar;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn status;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomeCompleto;
        private System.Windows.Forms.DataGridViewTextBoxColumn funcao;
        private System.Windows.Forms.DataGridViewTextBoxColumn estadoCivil;
        private System.Windows.Forms.DataGridViewTextBoxColumn celular;
        private System.Windows.Forms.DataGridViewTextBoxColumn endereco;
        private System.Windows.Forms.DataGridViewTextBoxColumn bairro;
        private System.Windows.Forms.DataGridViewTextBoxColumn sexo;
    }
}