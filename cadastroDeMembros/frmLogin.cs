﻿using System;
using System.Windows.Forms;
using Gyntech.DTO;
using Gyntech.Model;


namespace GyntechUMB
{
    public partial class frmLogin : Form
    {
        #region Programa Principal
        public frmLogin()
        {
            InitializeComponent();
            SetupMemberBankModel smbModel = new SetupMemberBankModel();

            smbModel.RestaurarDBPadraoCasoNaoExista();
        }

        //Botão Enter
        private void btnEnter_Click(object sender, EventArgs e)
        {
            fazerlogin(0);
        }

        private void btnNovo_Click(object sender, EventArgs e)
        {
            fazerlogin(1);
        }
        #endregion

        #region Função
        //Fazer login
        private bool fazerlogin(int tipo)
        {
            var palavraPasse = "admin";
            var login = txtLogin.Text;
            var senha = txtSenha.Text;
            if ((login == palavraPasse) && (senha == palavraPasse) && (login != "") && (senha != ""))
            {
                if(tipo == 0)
                {
                    abrirBemVindo();
                }
                else
                {
                    abrirNovo();
                }
                return true;                
            }
            else
            {
                MessageBox.Show("Login ou senha incorretos ou não preenchidos, tente novamente!");
                return false;
            }
        }

        //Abrir form bem vindo
        private void abrirBemVindo()
        {
            if (this.Visible == true)
            {
                this.Visible = false;
                frmBemVindo fbv = new frmBemVindo();
                fbv.ShowDialog();
                this.Close();
            }
        }

        //Abrir Novo Cadastro de Usuário
        private void abrirNovo()
        {
            if (this.Visible == true)
            {
                this.Visible = false;
                frmNewUser fnu = new frmNewUser();
                fnu.ShowDialog();
                this.Close();
            }
        }
        #endregion
    }
}