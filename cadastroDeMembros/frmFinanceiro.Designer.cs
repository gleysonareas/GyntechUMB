﻿using System;
using System.Windows.Forms;

namespace GyntechUMB
{
    partial class frmFinanceiro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmFinanceiro));
            this.lblReceitasDespesas = new System.Windows.Forms.Label();
            this.gdEntSai = new System.Windows.Forms.DataGridView();
            this.btnLancar = new System.Windows.Forms.Button();
            this.btnSair = new System.Windows.Forms.Button();
            this.pnlLancamentos = new System.Windows.Forms.Panel();
            this.txtId = new System.Windows.Forms.TextBox();
            this.lblId = new System.Windows.Forms.Label();
            this.txtTot = new System.Windows.Forms.TextBox();
            this.lblTot = new System.Windows.Forms.Label();
            this.cbTpDesp = new System.Windows.Forms.ComboBox();
            this.lblTpDesp = new System.Windows.Forms.Label();
            this.txtTotSai = new System.Windows.Forms.TextBox();
            this.lblTotSai = new System.Windows.Forms.Label();
            this.gbTipoLanca = new System.Windows.Forms.GroupBox();
            this.rbtnSaida = new System.Windows.Forms.RadioButton();
            this.rbtnEntrada = new System.Windows.Forms.RadioButton();
            this.cbDiaSem = new System.Windows.Forms.ComboBox();
            this.lblDiaSemana = new System.Windows.Forms.Label();
            this.txtTotEnt = new System.Windows.Forms.TextBox();
            this.lblTotEnt = new System.Windows.Forms.Label();
            this.txtDescricao = new System.Windows.Forms.TextBox();
            this.lblDescricao = new System.Windows.Forms.Label();
            this.mkdDataLanc = new System.Windows.Forms.MaskedTextBox();
            this.lblDataLanc = new System.Windows.Forms.Label();
            this.cbTpRec = new System.Windows.Forms.ComboBox();
            this.lblTpRec = new System.Windows.Forms.Label();
            this.txtValor = new System.Windows.Forms.TextBox();
            this.lblValor = new System.Windows.Forms.Label();
            this.pnlBotoes = new System.Windows.Forms.Panel();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Lancamentos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Valor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Dia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Data = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Receita = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Despesa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Descricao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gdEntSai)).BeginInit();
            this.pnlLancamentos.SuspendLayout();
            this.gbTipoLanca.SuspendLayout();
            this.pnlBotoes.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblReceitasDespesas
            // 
            this.lblReceitasDespesas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblReceitasDespesas.Font = new System.Drawing.Font("Segoe Print", 25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReceitasDespesas.ForeColor = System.Drawing.Color.DarkCyan;
            this.lblReceitasDespesas.Location = new System.Drawing.Point(0, 0);
            this.lblReceitasDespesas.Name = "lblReceitasDespesas";
            this.lblReceitasDespesas.Size = new System.Drawing.Size(756, 61);
            this.lblReceitasDespesas.TabIndex = 0;
            this.lblReceitasDespesas.Text = "Receitas e Despesas";
            this.lblReceitasDespesas.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // gdEntSai
            // 
            this.gdEntSai.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.gdEntSai.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gdEntSai.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.Lancamentos,
            this.Valor,
            this.Dia,
            this.Data,
            this.Receita,
            this.Despesa,
            this.Descricao});
            this.gdEntSai.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.gdEntSai.Location = new System.Drawing.Point(376, 66);
            this.gdEntSai.Name = "gdEntSai";
            this.gdEntSai.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gdEntSai.Size = new System.Drawing.Size(381, 394);
            this.gdEntSai.TabIndex = 4;
            this.gdEntSai.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gdEntSai_CellDoubleClick);
            // 
            // btnLancar
            // 
            this.btnLancar.Location = new System.Drawing.Point(1, 0);
            this.btnLancar.Name = "btnLancar";
            this.btnLancar.Size = new System.Drawing.Size(137, 32);
            this.btnLancar.TabIndex = 0;
            this.btnLancar.Text = "Lançar";
            this.btnLancar.UseVisualStyleBackColor = true;
            this.btnLancar.Click += new System.EventHandler(this.btnLancar_Click);
            // 
            // btnSair
            // 
            this.btnSair.Location = new System.Drawing.Point(144, 0);
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(137, 32);
            this.btnSair.TabIndex = 1;
            this.btnSair.Text = "Sair";
            this.btnSair.UseVisualStyleBackColor = true;
            this.btnSair.Click += new System.EventHandler(this.btnSair_Click);
            // 
            // pnlLancamentos
            // 
            this.pnlLancamentos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlLancamentos.Controls.Add(this.txtId);
            this.pnlLancamentos.Controls.Add(this.lblId);
            this.pnlLancamentos.Controls.Add(this.txtTot);
            this.pnlLancamentos.Controls.Add(this.lblTot);
            this.pnlLancamentos.Controls.Add(this.cbTpDesp);
            this.pnlLancamentos.Controls.Add(this.lblTpDesp);
            this.pnlLancamentos.Controls.Add(this.txtTotSai);
            this.pnlLancamentos.Controls.Add(this.lblTotSai);
            this.pnlLancamentos.Controls.Add(this.gbTipoLanca);
            this.pnlLancamentos.Controls.Add(this.cbDiaSem);
            this.pnlLancamentos.Controls.Add(this.lblDiaSemana);
            this.pnlLancamentos.Controls.Add(this.txtTotEnt);
            this.pnlLancamentos.Controls.Add(this.lblTotEnt);
            this.pnlLancamentos.Controls.Add(this.txtDescricao);
            this.pnlLancamentos.Controls.Add(this.lblDescricao);
            this.pnlLancamentos.Controls.Add(this.mkdDataLanc);
            this.pnlLancamentos.Controls.Add(this.lblDataLanc);
            this.pnlLancamentos.Controls.Add(this.cbTpRec);
            this.pnlLancamentos.Controls.Add(this.lblTpRec);
            this.pnlLancamentos.Controls.Add(this.txtValor);
            this.pnlLancamentos.Controls.Add(this.lblValor);
            this.pnlLancamentos.Location = new System.Drawing.Point(1, 66);
            this.pnlLancamentos.Name = "pnlLancamentos";
            this.pnlLancamentos.Size = new System.Drawing.Size(369, 394);
            this.pnlLancamentos.TabIndex = 1;
            // 
            // txtId
            // 
            this.txtId.Enabled = false;
            this.txtId.Location = new System.Drawing.Point(129, 82);
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(38, 20);
            this.txtId.TabIndex = 2;
            // 
            // lblId
            // 
            this.lblId.AutoSize = true;
            this.lblId.Location = new System.Drawing.Point(107, 85);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(16, 13);
            this.lblId.TabIndex = 21;
            this.lblId.Text = "Id";
            // 
            // txtTot
            // 
            this.txtTot.Enabled = false;
            this.txtTot.Location = new System.Drawing.Point(255, 334);
            this.txtTot.Name = "txtTot";
            this.txtTot.Size = new System.Drawing.Size(100, 20);
            this.txtTot.TabIndex = 9;
            // 
            // lblTot
            // 
            this.lblTot.AutoSize = true;
            this.lblTot.Location = new System.Drawing.Point(218, 337);
            this.lblTot.Name = "lblTot";
            this.lblTot.Size = new System.Drawing.Size(31, 13);
            this.lblTot.TabIndex = 19;
            this.lblTot.Text = "Total";
            // 
            // cbTpDesp
            // 
            this.cbTpDesp.FormattingEnabled = true;
            this.cbTpDesp.Items.AddRange(new object[] {
            "Água",
            "Luz",
            "Comunicação",
            "Aluguel",
            "Manutenção de Patrimônios",
            "Limpeza",
            "Outros"});
            this.cbTpDesp.Location = new System.Drawing.Point(231, 39);
            this.cbTpDesp.Name = "cbTpDesp";
            this.cbTpDesp.Size = new System.Drawing.Size(124, 21);
            this.cbTpDesp.TabIndex = 4;
            this.cbTpDesp.Visible = false;
            this.cbTpDesp.Click += new System.EventHandler(this.cbTpDesp_Click);
            // 
            // lblTpDesp
            // 
            this.lblTpDesp.AutoSize = true;
            this.lblTpDesp.Location = new System.Drawing.Point(135, 42);
            this.lblTpDesp.Name = "lblTpDesp";
            this.lblTpDesp.Size = new System.Drawing.Size(90, 13);
            this.lblTpDesp.TabIndex = 1;
            this.lblTpDesp.Text = "Tipo De Despesa";
            this.lblTpDesp.Visible = false;
            // 
            // txtTotSai
            // 
            this.txtTotSai.Enabled = false;
            this.txtTotSai.Location = new System.Drawing.Point(107, 360);
            this.txtTotSai.Name = "txtTotSai";
            this.txtTotSai.Size = new System.Drawing.Size(101, 20);
            this.txtTotSai.TabIndex = 8;
            // 
            // lblTotSai
            // 
            this.lblTotSai.AutoSize = true;
            this.lblTotSai.Location = new System.Drawing.Point(18, 363);
            this.lblTotSai.Name = "lblTotSai";
            this.lblTotSai.Size = new System.Drawing.Size(83, 13);
            this.lblTotSai.TabIndex = 16;
            this.lblTotSai.Text = "Total de Saídas";
            // 
            // gbTipoLanca
            // 
            this.gbTipoLanca.Controls.Add(this.rbtnSaida);
            this.gbTipoLanca.Controls.Add(this.rbtnEntrada);
            this.gbTipoLanca.Location = new System.Drawing.Point(2, 2);
            this.gbTipoLanca.Name = "gbTipoLanca";
            this.gbTipoLanca.Size = new System.Drawing.Size(123, 73);
            this.gbTipoLanca.TabIndex = 0;
            this.gbTipoLanca.TabStop = false;
            this.gbTipoLanca.Text = "Tipo do Lançamento:";
            // 
            // rbtnSaida
            // 
            this.rbtnSaida.AutoSize = true;
            this.rbtnSaida.Location = new System.Drawing.Point(29, 45);
            this.rbtnSaida.Name = "rbtnSaida";
            this.rbtnSaida.Size = new System.Drawing.Size(54, 17);
            this.rbtnSaida.TabIndex = 1;
            this.rbtnSaida.TabStop = true;
            this.rbtnSaida.Text = "Saída";
            this.rbtnSaida.UseVisualStyleBackColor = true;
            this.rbtnSaida.CheckedChanged += new System.EventHandler(this.rbtnSaida_CheckedChanged);
            // 
            // rbtnEntrada
            // 
            this.rbtnEntrada.AutoSize = true;
            this.rbtnEntrada.Location = new System.Drawing.Point(29, 22);
            this.rbtnEntrada.Name = "rbtnEntrada";
            this.rbtnEntrada.Size = new System.Drawing.Size(62, 17);
            this.rbtnEntrada.TabIndex = 0;
            this.rbtnEntrada.TabStop = true;
            this.rbtnEntrada.Text = "Entrada";
            this.rbtnEntrada.UseVisualStyleBackColor = true;
            this.rbtnEntrada.CheckedChanged += new System.EventHandler(this.rbtnEntrada_CheckedChanged);
            // 
            // cbDiaSem
            // 
            this.cbDiaSem.FormattingEnabled = true;
            this.cbDiaSem.Items.AddRange(new object[] {
            "Domingo",
            "Segunda-Feira",
            "Terça-Feira",
            "Quarta-Feira",
            "Quinta-Feira",
            "Sexta-Feira",
            "Sábado"});
            this.cbDiaSem.Location = new System.Drawing.Point(261, 108);
            this.cbDiaSem.Name = "cbDiaSem";
            this.cbDiaSem.Size = new System.Drawing.Size(94, 21);
            this.cbDiaSem.TabIndex = 5;
            // 
            // lblDiaSemana
            // 
            this.lblDiaSemana.AutoSize = true;
            this.lblDiaSemana.Location = new System.Drawing.Point(173, 111);
            this.lblDiaSemana.Name = "lblDiaSemana";
            this.lblDiaSemana.Size = new System.Drawing.Size(82, 13);
            this.lblDiaSemana.TabIndex = 11;
            this.lblDiaSemana.Text = "Dia Da Semana";
            // 
            // txtTotEnt
            // 
            this.txtTotEnt.Enabled = false;
            this.txtTotEnt.Location = new System.Drawing.Point(107, 334);
            this.txtTotEnt.Name = "txtTotEnt";
            this.txtTotEnt.Size = new System.Drawing.Size(101, 20);
            this.txtTotEnt.TabIndex = 7;
            // 
            // lblTotEnt
            // 
            this.lblTotEnt.AutoSize = true;
            this.lblTotEnt.Location = new System.Drawing.Point(10, 337);
            this.lblTotEnt.Name = "lblTotEnt";
            this.lblTotEnt.Size = new System.Drawing.Size(91, 13);
            this.lblTotEnt.TabIndex = 8;
            this.lblTotEnt.Text = "Total de Entradas";
            // 
            // txtDescricao
            // 
            this.txtDescricao.Location = new System.Drawing.Point(11, 162);
            this.txtDescricao.Multiline = true;
            this.txtDescricao.Name = "txtDescricao";
            this.txtDescricao.Size = new System.Drawing.Size(344, 166);
            this.txtDescricao.TabIndex = 6;
            this.txtDescricao.Leave += new System.EventHandler(this.txtDescricao_Leave);
            // 
            // lblDescricao
            // 
            this.lblDescricao.AutoSize = true;
            this.lblDescricao.Location = new System.Drawing.Point(8, 146);
            this.lblDescricao.Name = "lblDescricao";
            this.lblDescricao.Size = new System.Drawing.Size(55, 13);
            this.lblDescricao.TabIndex = 6;
            this.lblDescricao.Text = "Descrição";
            // 
            // mkdDataLanc
            // 
            this.mkdDataLanc.Location = new System.Drawing.Point(286, 82);
            this.mkdDataLanc.Mask = "00/00/0000";
            this.mkdDataLanc.Name = "mkdDataLanc";
            this.mkdDataLanc.Size = new System.Drawing.Size(69, 20);
            this.mkdDataLanc.TabIndex = 3;
            this.mkdDataLanc.ValidatingType = typeof(System.DateTime);
            // 
            // lblDataLanc
            // 
            this.lblDataLanc.AutoSize = true;
            this.lblDataLanc.Location = new System.Drawing.Point(173, 85);
            this.lblDataLanc.Name = "lblDataLanc";
            this.lblDataLanc.Size = new System.Drawing.Size(107, 13);
            this.lblDataLanc.TabIndex = 4;
            this.lblDataLanc.Text = "Data do Lançamento";
            // 
            // cbTpRec
            // 
            this.cbTpRec.FormattingEnabled = true;
            this.cbTpRec.Items.AddRange(new object[] {
            "Oferta",
            "Dízimo",
            "Oferta Especial",
            "Outros"});
            this.cbTpRec.Location = new System.Drawing.Point(231, 12);
            this.cbTpRec.Name = "cbTpRec";
            this.cbTpRec.Size = new System.Drawing.Size(124, 21);
            this.cbTpRec.TabIndex = 0;
            this.cbTpRec.Visible = false;
            this.cbTpRec.Click += new System.EventHandler(this.cbTpRec_Click);
            // 
            // lblTpRec
            // 
            this.lblTpRec.AutoSize = true;
            this.lblTpRec.Location = new System.Drawing.Point(140, 15);
            this.lblTpRec.Name = "lblTpRec";
            this.lblTpRec.Size = new System.Drawing.Size(85, 13);
            this.lblTpRec.TabIndex = 2;
            this.lblTpRec.Text = "Tipo De Receita";
            this.lblTpRec.Visible = false;
            // 
            // txtValor
            // 
            this.txtValor.Location = new System.Drawing.Point(67, 108);
            this.txtValor.Name = "txtValor";
            this.txtValor.Size = new System.Drawing.Size(100, 20);
            this.txtValor.TabIndex = 4;
            this.txtValor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtValor_KeyPress);
            // 
            // lblValor
            // 
            this.lblValor.AutoSize = true;
            this.lblValor.Location = new System.Drawing.Point(30, 111);
            this.lblValor.Name = "lblValor";
            this.lblValor.Size = new System.Drawing.Size(31, 13);
            this.lblValor.TabIndex = 0;
            this.lblValor.Text = "Valor";
            // 
            // pnlBotoes
            // 
            this.pnlBotoes.Controls.Add(this.btnLancar);
            this.pnlBotoes.Controls.Add(this.btnSair);
            this.pnlBotoes.Location = new System.Drawing.Point(0, 466);
            this.pnlBotoes.Name = "pnlBotoes";
            this.pnlBotoes.Size = new System.Drawing.Size(758, 35);
            this.pnlBotoes.TabIndex = 2;
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.Width = 40;
            // 
            // Lancamentos
            // 
            this.Lancamentos.DataPropertyName = "Lancamentos";
            this.Lancamentos.HeaderText = "Lançamentos";
            this.Lancamentos.Name = "Lancamentos";
            this.Lancamentos.Width = 75;
            // 
            // Valor
            // 
            this.Valor.DataPropertyName = "Valor";
            this.Valor.HeaderText = "Valor";
            this.Valor.Name = "Valor";
            this.Valor.Width = 70;
            // 
            // Dia
            // 
            this.Dia.DataPropertyName = "Dia";
            this.Dia.HeaderText = "Dia";
            this.Dia.Name = "Dia";
            this.Dia.Width = 80;
            // 
            // Data
            // 
            this.Data.DataPropertyName = "Data";
            this.Data.HeaderText = "Data";
            this.Data.Name = "Data";
            this.Data.Width = 70;
            // 
            // Receita
            // 
            this.Receita.DataPropertyName = "Receita";
            this.Receita.HeaderText = "Receita";
            this.Receita.Name = "Receita";
            // 
            // Despesa
            // 
            this.Despesa.DataPropertyName = "Despesa";
            this.Despesa.HeaderText = "Despesa";
            this.Despesa.Name = "Despesa";
            // 
            // Descricao
            // 
            this.Descricao.DataPropertyName = "Descricao";
            this.Descricao.HeaderText = "Descrição";
            this.Descricao.Name = "Descricao";
            // 
            // frmFinanceiro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(758, 501);
            this.ControlBox = false;
            this.Controls.Add(this.pnlLancamentos);
            this.Controls.Add(this.pnlBotoes);
            this.Controls.Add(this.gdEntSai);
            this.Controls.Add(this.lblReceitasDespesas);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(300, 141);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmFinanceiro";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Financeiro";
            this.Activated += new System.EventHandler(this.frmEntSai_Activated);
            ((System.ComponentModel.ISupportInitialize)(this.gdEntSai)).EndInit();
            this.pnlLancamentos.ResumeLayout(false);
            this.pnlLancamentos.PerformLayout();
            this.gbTipoLanca.ResumeLayout(false);
            this.gbTipoLanca.PerformLayout();
            this.pnlBotoes.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblReceitasDespesas;
        private System.Windows.Forms.DataGridView gdEntSai;
        private System.Windows.Forms.Button btnSair;
        private System.Windows.Forms.Button btnLancar;
        private Panel pnlLancamentos;
        private TextBox txtId;
        private Label lblId;
        private TextBox txtTot;
        private Label lblTot;
        private ComboBox cbTpDesp;
        private Label lblTpDesp;
        private TextBox txtTotSai;
        private Label lblTotSai;
        private GroupBox gbTipoLanca;
        private RadioButton rbtnSaida;
        private RadioButton rbtnEntrada;
        private ComboBox cbDiaSem;
        private Label lblDiaSemana;
        private TextBox txtTotEnt;
        private Label lblTotEnt;
        private TextBox txtDescricao;
        private Label lblDescricao;
        private MaskedTextBox mkdDataLanc;
        private Label lblDataLanc;
        private ComboBox cbTpRec;
        private Label lblTpRec;
        private TextBox txtValor;
        private Label lblValor;
        private Panel pnlBotoes;
        private DataGridViewTextBoxColumn Id;
        private DataGridViewTextBoxColumn Lancamentos;
        private DataGridViewTextBoxColumn Valor;
        private DataGridViewTextBoxColumn Dia;
        private DataGridViewTextBoxColumn Data;
        private DataGridViewTextBoxColumn Receita;
        private DataGridViewTextBoxColumn Despesa;
        private DataGridViewTextBoxColumn Descricao;
    }
}