﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;
using Gyntech.DTO;
using Gyntech.Model;

namespace GyntechUMB
{
    public partial class frmFinanceiro : Form
    {
        FinanceiroDTO fDTO = new FinanceiroDTO();
        FinanceiroModel fModel = new FinanceiroModel();

        #region Programa Principal
        public frmFinanceiro()
        {
            InitializeComponent();
        }

        private void frmEntSai_Activated(object sender, EventArgs e)
        {
            carregarCaixa();
            receberReceitas();
            receberDespesas();
            calcularCaixa();
        }

        #region Lógica das Caixas de Texto    
        //Validação da combo box Receita
        private void cbTpRec_Click(object sender, EventArgs e)
        {
            if (cbTpRec.SelectedIndex == -1)
                cbTpRec.ValueMember = "none";
        }

        //Validação da combo box Despesa
        private void cbTpDesp_Click(object sender, EventArgs e)
        {
            if (cbTpDesp.SelectedIndex == -1)
                cbTpDesp.ValueMember = "none";
        }

        //Valor
        private void txtValor_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((char.IsLetter(e.KeyChar)) && (e.KeyChar != (char)8))
                e.Handled = true;
        }

        //Descrição
        private void txtDescricao_Leave(object sender, EventArgs e)
        {
            if (txtDescricao.Text == "")
            {
                txtDescricao.ForeColor = Color.LightGray;
                txtDescricao.Text = "O campo DESCRIÇÃO necessita ser preenchido";
            }
        }

        #endregion

        #region Lógica dos Botões
        //Tipo de Lançamento
        private void rbtnEntrada_CheckedChanged(object sender, EventArgs e)
        {
            trocarTipo();
        }

        private void rbtnSaida_CheckedChanged(object sender, EventArgs e)
        {
            trocarTipo();
        }

        //Botão Sair que retorna para janela anterior
        private void btnSair_Click(object sender, EventArgs e)
        {
            fecharJanela();
        }

        //Botão Lançar
        private void btnLancar_Click(object sender, EventArgs e)
        {
            List<string> mensagem = ValidarTela();

            if (mensagem.Count == 0)
            {
                salvarNoBanco();
                carregarCaixa();
                receberReceitas();
                receberDespesas();
                calcularCaixa();
                limparCampo();
            }
            else
            {
                string msg = "";
                foreach (var m in mensagem)
                {
                    msg = msg + Environment.NewLine + m;
                }
                MessageBox.Show(msg, "Erros no preenchimento", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        //Duplo click no grid
        private void gdEntSai_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            carregarDoBanco();
        }

        #endregion

        #endregion

        #region Funções
        private string CleanFieldsMKD(string text)
        {
            string result = string.Empty;
            result = text.Replace(" ", "").Replace("_", "").Replace("R$", "");
            return result;
        }

        //Função limpa campo
        private void limparCampo()
        {
            cbTpRec.ResetText();
            cbTpDesp.ResetText();
            mkdDataLanc.Clear();
            txtValor.Clear();
            cbDiaSem.ResetText();
            txtDescricao.Clear();
        }

        //Função esconde campo
        private bool trocarTipo()
        {
            if (rbtnEntrada.Checked == true)
            {
                lblTpRec.Visible = true;
                cbTpRec.Visible = true;
                lblTpDesp.Visible = false;
                cbTpDesp.Visible = false;
                cbTpDesp.ResetText();
                return true;
            }
            else
            {
                lblTpRec.Visible = false;
                cbTpRec.Visible = false;
                lblTpDesp.Visible = true;
                cbTpDesp.Visible = true;
                cbTpRec.ResetText();
                return false;
            }
        }

        //Função de validação das caixas de texto
        private List<string> ValidarTela()
        {
            List<string> erros = new List<string>();

            //Validação do campo tipo de receita
            if ((string.IsNullOrWhiteSpace(cbTpRec.Text)) && (rbtnEntrada.Checked == false) && (rbtnSaida.Checked == false))
                erros.Add("Receita não selecionada!");

            //Validação do campo tipo de despesa
            if ((string.IsNullOrWhiteSpace(cbTpDesp.Text)) && (rbtnSaida.Checked == false) && (rbtnEntrada.Checked == false))
                erros.Add("Despesa não selecionada!");

            //Validação do campo valor
            if (string.IsNullOrWhiteSpace(txtValor.Text))
                erros.Add("Valor inválido!");

            //Validação do campo data lançamento
            DateTime dataLanca;
            if (DateTime.TryParse(mkdDataLanc.Text, out dataLanca))
            {
                if (dataLanca.Year < 1900 || dataLanca.Year > DateTime.Today.Year)
                {
                    erros.Add("Data de lançamento inválida!");
                }
            }

            else
            {
                erros.Add("Data de lançamento inválida!");
            }

            //Validação do campo dia da semana
            if (string.IsNullOrWhiteSpace(cbDiaSem.Text))
                erros.Add("Dia da semana não selecionado!");

            //Validação do campo descrição
            if (string.IsNullOrWhiteSpace(txtDescricao.Text))
                erros.Add("Descrição inválida!");

            return erros;
        }

        //Função que realiza o INSERT no banco
        private void salvarNoBanco()
        {
            try
            {
                //Lançamento
                if (rbtnEntrada.Checked == true)
                    fDTO.Lancamentos = "Entrada";
                else
                    fDTO.Lancamentos = "Saída";
                fDTO.Receita = cbTpRec.Text;
                fDTO.Despesa = cbTpDesp.Text;
                fDTO.Data = Convert.ToDateTime(mkdDataLanc.Text);
                fDTO.Dia = cbDiaSem.Text;
                fDTO.Valor = txtValor.Text;
                fDTO.Descricao = txtDescricao.Text;

                int idSelecionado = int.Parse(gdEntSai.SelectedRows[0].Cells[0].Value.ToString());
                if (idSelecionado == 0)
                {
                    int fin = fModel.Inserir(fDTO);
                    if (fin > 0)
                        MessageBox.Show("Os Dados foram salvos com sucesso");
                }
                else
                {
                    int fin = fModel.Atualizar(fDTO);
                    if (fin > 0)
                        MessageBox.Show("Os Dados foram alterados com sucesso");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ocorreu um erro ao salvar as informações no banco ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Função que realiza o SELECT no banco
        private void carregarCaixa()
        {
            try
            {
                IList<FinanceiroDTO> listaFinanceira = new List<FinanceiroDTO>();
                listaFinanceira = fModel.CarregarFinancas();
                gdEntSai.AutoGenerateColumns = false;
                gdEntSai.DataSource = listaFinanceira;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ocorreu um erro ao carregar as informações do banco de dados: ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Função editar campo
        private void carregarDoBanco()
        {
            try
            {
                if (gdEntSai.SelectedRows.Count > 0)
                {
                    int idSelecionado = int.Parse(gdEntSai.SelectedRows[0].Cells[0].Value.ToString());
                    IList<FinanceiroDTO> listaFinanceiraDTO = new List<FinanceiroDTO>();
                    listaFinanceiraDTO = fModel.CarregarDoBanco(idSelecionado);

                    foreach (var iten in listaFinanceiraDTO)
                    {
                        if (iten.Lancamentos == "Entrada")
                            rbtnEntrada.Checked = true;
                        else
                            rbtnSaida.Checked = true;
                        cbTpRec.Text = iten.Receita;
                        cbTpDesp.Text = iten.Despesa;
                        mkdDataLanc.Text = Convert.ToString(iten.Data);
                        cbDiaSem.Text = iten.Dia;
                        txtValor.Text = iten.Valor;
                        txtDescricao.Text = iten.Descricao;
                        txtId.Text = Convert.ToString(iten.Id);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro ao carregar os dados financeiros para edição", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Função que retorna somente o valor das RECEITAS
        private void receberReceitas()
        {
            try
            {
                if (gdEntSai.SelectedRows.Count > 0)
                {
                    IList<FinanceiroDTO> listaReceitas = new List<FinanceiroDTO>();
                    listaReceitas = fModel.CarregarFinancas();
                    double tot = 0;
                    foreach (var item in listaReceitas)
                    {
                        string receita = Convert.ToString(item.Receita);
                        if (receita != "")
                        {
                            double val = double.Parse(item.Valor);
                            tot = tot + val;
                            txtTotEnt.Text = Convert.ToString(tot);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro ao receber dados financeiros", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Função que retorna somente o valor das DESPESAS
        private void receberDespesas()
        {
            try
            {
                if (gdEntSai.SelectedRows.Count > 0)
                {
                    IList<FinanceiroDTO> listaReceitas = new List<FinanceiroDTO>();
                    listaReceitas = fModel.CarregarFinancas();
                    double tot = 0;
                    foreach (var item in listaReceitas)
                    {
                        string despesa = Convert.ToString(item.Despesa);
                        if (despesa != "")
                        {
                            double val = double.Parse(item.Valor);
                            tot = tot + val;
                            txtTotSai.Text = Convert.ToString(tot);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro ao receber dados financeiros", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Função do calculo de total em caixa
        private void calcularCaixa()
        {
            if ((txtTotEnt.Text != "") && (txtTotSai.Text != ""))
            {
                double entrada = double.Parse(txtTotEnt.Text);
                double saida = double.Parse(txtTotSai.Text);
                double total = entrada - saida;
                if (total > 0)
                    txtTot.ForeColor = Color.Green;
                else if (total == 0)
                    txtTot.ForeColor = Color.Yellow;
                else
                    txtTot.ForeColor = Color.Red;

                txtTot.Text = total.ToString();
            }
            else if (txtTotEnt.Text != "")
            {
                txtTot.ForeColor = Color.Green;
                txtTot.Text = txtTotEnt.Text;
            }
            else
            {
                txtTot.ForeColor = Color.Red;
                txtTot.Text = txtTotSai.Text;
            }
        }

        //Função que volta para FORM anterior
        private void fecharJanela()
        {
            this.Close();
        }
        #endregion
    }
}
