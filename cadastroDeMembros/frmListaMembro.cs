﻿using System;
using System.Windows.Forms;
using Gyntech.DTO;
using Gyntech.Model;
using System.Collections.Generic;

namespace GyntechUMB
{
    public partial class frmListaMembro : Form
    {
        #region Programa Principal
        MembroDTO mDto = new MembroDTO();
        MembroModel mModel = new MembroModel();

        public frmListaMembro()
        {
            InitializeComponent();
        }
        private void frmMemberBank_Activated(object sender, EventArgs e)
        {
            carregarMembros();
        }

        //Botão Novo
        private void btnNovo_Click(object sender, EventArgs e)
        {
            novoMembro();
        }

        //Duplo Click no Grid
        private void gdMembros_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            editarMembros();
        }

        //Botão Imprimir
        private void btnImprimir_Click(object sender, EventArgs e)
        {
            imprimirRelatorio();
        }

        private void gdMembros_KeyDown(object sender, KeyEventArgs e)
        {
            //Reintegrar Membros
            if (e.KeyCode == Keys.Space)
                reintegrarMembros();

            //Inativar Membros
            if (e.KeyCode == Keys.Delete)
                inativarMembros();

            //Carregar dados no outro FORM
            if (e.KeyCode == Keys.F5)
                editarMembros();

            //Atalho para novo 
            if (e.KeyCode == Keys.F9)
                novoMembro();

            //Atalho para imprirmir
        }
        #endregion

        #region Funções
        //Carregar dados do banco no outro FORM
        private void editarMembros()
        {
            if (gdMembros.SelectedRows.Count > 0)
            {
                int idSelecionado = int.Parse(gdMembros.SelectedRows[0].Cells[0].Value.ToString());
                this.Close();
                frmCadastro fc = new frmCadastro();
                fc.idMembroSelecionado = idSelecionado;
                fc.Show();
            }
            else
            {
                MessageBox.Show("Não existe linha selecionada para Editar!");
            }
        }

        //Abrir novo form de cadastro
        private void novoMembro()
        {
            this.Close();
            frmCadastro fc = new frmCadastro();
            fc.Show();
        }

        //Imprimir Relatório em PDF
        private void imprimirRelatorio()
        {

        }

        //Função que carrega os membros
        private void carregarMembros()
        {
            try
            {
                IList<MembroDTO> listaMembros = new List<MembroDTO>();
                listaMembros = mModel.CarregarMembros();
                gdMembros.AutoGenerateColumns = false;
                gdMembros.DataSource = listaMembros;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ocorreu um erro ao carregar os dados do banco: ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Inativar membros no Banco
        private void inativarMembros()
        {
            try
            {
                if ((gdMembros.SelectedRows.Count > 0) && (MessageBox.Show("Tem certeza que deseja inativar este cadastro?", "Cuidado", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes))
                {
                    int idSelecionado = int.Parse(gdMembros.SelectedRows[0].Cells[0].Value.ToString());
                    mDto.Id = idSelecionado;
                    mDto.Status = "Inativo";
                    int mb = mModel.AlterarStatus(mDto);
                    carregarMembros();
                }
                else
                {
                    MessageBox.Show("Não existe linha selecionada para inativar!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ocorreu um erro ao tentar inativar este membro: ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Reintegrar membros
        private void reintegrarMembros()
        {
            try
            {
                if ((gdMembros.SelectedRows.Count > 0) && (MessageBox.Show("Tem certeza que deseja reintegrar este cadastro?", "Cuidado", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes))
                {
                    int idSelecionado = int.Parse(gdMembros.SelectedRows[0].Cells[0].Value.ToString());
                    mDto.Id = idSelecionado;
                    mDto.Status = "Ativo";
                    int mb = mModel.AlterarStatus(mDto);
                    carregarMembros();
                }
                else
                {
                    MessageBox.Show("Não existe linha selecionada para reintegrar!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ocorreu um erro ao tentar reintegrar este Membro: ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion
    }
}
