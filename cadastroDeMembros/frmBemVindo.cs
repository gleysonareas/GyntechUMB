﻿using System;
using System.Windows.Forms;

namespace GyntechUMB
{
    public partial class frmBemVindo : Form
    {
        #region Programa Principal

        public frmBemVindo()
        {
            InitializeComponent();
        }
        //Botão Home
        private void btnHome_Click(object sender, EventArgs e)
        {
            AbriFechaForm(0);
        }
        //Botão Lista
        private void btnLista_Click(object sender, EventArgs e)
        {
            AbriFechaForm(1);
        }
        //Botão Cadastro
        private void btnCadastro_Click(object sender, EventArgs e)
        {
            AbriFechaForm(2);
        }
        //Botão Financeiro
        private void btnFinanceiro_Click(object sender, EventArgs e)
        {
            AbriFechaForm(3);
        }
        //Botão Ação Social
        private void btnAcaoSocial_Click(object sender, EventArgs e)
        {
            AbriFechaForm(4);
        }
        #endregion

        #region Funções

        private void AbriFechaForm(int selecioneCaso)
        {
            frmListaMembro flm = new frmListaMembro();
            frmCadastro fc = new frmCadastro();
            frmFinanceiro ff = new frmFinanceiro();
            frmAcaoSocial fas = new frmAcaoSocial();

            switch (selecioneCaso)
            {
                default:
                    flm.Close();
                    fc.Close();
                    ff.Close();
                    fas.Close();
                    break;

                case 1:
                    flm.Show();
                    fc.Close();
                    ff.Close();
                    fas.Close();
                    break;

                case 2:
                    flm.Close();
                    fc.Show();
                    ff.Close();
                    fas.Close();
                    break;

                case 3:
                    flm.Close();
                    fc.Close();
                    ff.Show();
                    fas.Close();
                    break;

                case 4:
                    flm.Close();
                    fc.Close();
                    ff.Close();
                    fas.Show();
                    break;
            }

            #endregion
        }
    }
}