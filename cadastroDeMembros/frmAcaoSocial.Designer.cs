﻿namespace GyntechUMB
{
    partial class frmAcaoSocial
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAcaoSocial));
            this.btnLancar = new System.Windows.Forms.Button();
            this.btnSair = new System.Windows.Forms.Button();
            this.gdAcSoc = new System.Windows.Forms.DataGridView();
            this.lblReceitasDespesas = new System.Windows.Forms.Label();
            this.pnlLancamentos = new System.Windows.Forms.Panel();
            this.txtId = new System.Windows.Forms.TextBox();
            this.lblId = new System.Windows.Forms.Label();
            this.mkdDtSai = new System.Windows.Forms.MaskedTextBox();
            this.mkdDtEnt = new System.Windows.Forms.MaskedTextBox();
            this.lblDataSaida = new System.Windows.Forms.Label();
            this.lblDataEntrada = new System.Windows.Forms.Label();
            this.txtFabricante = new System.Windows.Forms.TextBox();
            this.lblFabricante = new System.Windows.Forms.Label();
            this.mkdValidade = new System.Windows.Forms.MaskedTextBox();
            this.lblValidade = new System.Windows.Forms.Label();
            this.txtQtd = new System.Windows.Forms.TextBox();
            this.lblQtd = new System.Windows.Forms.Label();
            this.txtNomeProduto = new System.Windows.Forms.TextBox();
            this.lblNomeProduto = new System.Windows.Forms.Label();
            this.cbTpSai = new System.Windows.Forms.ComboBox();
            this.lblTpSaida = new System.Windows.Forms.Label();
            this.cbTpEnt = new System.Windows.Forms.ComboBox();
            this.lblTpEntrada = new System.Windows.Forms.Label();
            this.txtDescricao = new System.Windows.Forms.TextBox();
            this.lblDescricao = new System.Windows.Forms.Label();
            this.gbTipoLanca = new System.Windows.Forms.GroupBox();
            this.rbtnSaida = new System.Windows.Forms.RadioButton();
            this.rbtnEntrada = new System.Windows.Forms.RadioButton();
            this.pnlBotoes = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.gdAcSoc)).BeginInit();
            this.pnlLancamentos.SuspendLayout();
            this.gbTipoLanca.SuspendLayout();
            this.pnlBotoes.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnLancar
            // 
            this.btnLancar.Location = new System.Drawing.Point(1, 0);
            this.btnLancar.Name = "btnLancar";
            this.btnLancar.Size = new System.Drawing.Size(137, 32);
            this.btnLancar.TabIndex = 0;
            this.btnLancar.Text = "Lançar";
            this.btnLancar.UseVisualStyleBackColor = true;
            this.btnLancar.Click += new System.EventHandler(this.btnLancar_Click);
            // 
            // btnSair
            // 
            this.btnSair.Location = new System.Drawing.Point(144, 0);
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(137, 32);
            this.btnSair.TabIndex = 1;
            this.btnSair.Text = "Sair";
            this.btnSair.UseVisualStyleBackColor = true;
            this.btnSair.Click += new System.EventHandler(this.btnSair_Click);
            // 
            // gdAcSoc
            // 
            this.gdAcSoc.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.gdAcSoc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gdAcSoc.Location = new System.Drawing.Point(372, 64);
            this.gdAcSoc.Name = "gdAcSoc";
            this.gdAcSoc.Size = new System.Drawing.Size(386, 394);
            this.gdAcSoc.TabIndex = 3;
            // 
            // lblReceitasDespesas
            // 
            this.lblReceitasDespesas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblReceitasDespesas.Font = new System.Drawing.Font("Segoe Print", 25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReceitasDespesas.ForeColor = System.Drawing.Color.DarkCyan;
            this.lblReceitasDespesas.Location = new System.Drawing.Point(0, 0);
            this.lblReceitasDespesas.Name = "lblReceitasDespesas";
            this.lblReceitasDespesas.Size = new System.Drawing.Size(758, 61);
            this.lblReceitasDespesas.TabIndex = 0;
            this.lblReceitasDespesas.Text = "Ação Social";
            this.lblReceitasDespesas.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlLancamentos
            // 
            this.pnlLancamentos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlLancamentos.Controls.Add(this.txtId);
            this.pnlLancamentos.Controls.Add(this.lblId);
            this.pnlLancamentos.Controls.Add(this.mkdDtSai);
            this.pnlLancamentos.Controls.Add(this.mkdDtEnt);
            this.pnlLancamentos.Controls.Add(this.lblDataSaida);
            this.pnlLancamentos.Controls.Add(this.lblDataEntrada);
            this.pnlLancamentos.Controls.Add(this.txtFabricante);
            this.pnlLancamentos.Controls.Add(this.lblFabricante);
            this.pnlLancamentos.Controls.Add(this.mkdValidade);
            this.pnlLancamentos.Controls.Add(this.lblValidade);
            this.pnlLancamentos.Controls.Add(this.txtQtd);
            this.pnlLancamentos.Controls.Add(this.lblQtd);
            this.pnlLancamentos.Controls.Add(this.txtNomeProduto);
            this.pnlLancamentos.Controls.Add(this.lblNomeProduto);
            this.pnlLancamentos.Controls.Add(this.cbTpSai);
            this.pnlLancamentos.Controls.Add(this.lblTpSaida);
            this.pnlLancamentos.Controls.Add(this.cbTpEnt);
            this.pnlLancamentos.Controls.Add(this.lblTpEntrada);
            this.pnlLancamentos.Controls.Add(this.txtDescricao);
            this.pnlLancamentos.Controls.Add(this.lblDescricao);
            this.pnlLancamentos.Controls.Add(this.gbTipoLanca);
            this.pnlLancamentos.Location = new System.Drawing.Point(0, 64);
            this.pnlLancamentos.Name = "pnlLancamentos";
            this.pnlLancamentos.Size = new System.Drawing.Size(366, 394);
            this.pnlLancamentos.TabIndex = 1;
            // 
            // txtId
            // 
            this.txtId.Enabled = false;
            this.txtId.Location = new System.Drawing.Point(320, 107);
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(38, 20);
            this.txtId.TabIndex = 2;
            // 
            // lblId
            // 
            this.lblId.AutoSize = true;
            this.lblId.Location = new System.Drawing.Point(298, 110);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(16, 13);
            this.lblId.TabIndex = 27;
            this.lblId.Text = "Id";
            // 
            // mkdDtSai
            // 
            this.mkdDtSai.Location = new System.Drawing.Point(289, 185);
            this.mkdDtSai.Mask = "00/00/0000";
            this.mkdDtSai.Name = "mkdDtSai";
            this.mkdDtSai.Size = new System.Drawing.Size(69, 20);
            this.mkdDtSai.TabIndex = 8;
            this.mkdDtSai.ValidatingType = typeof(System.DateTime);
            // 
            // mkdDtEnt
            // 
            this.mkdDtEnt.Location = new System.Drawing.Point(104, 185);
            this.mkdDtEnt.Mask = "00/00/0000";
            this.mkdDtEnt.Name = "mkdDtEnt";
            this.mkdDtEnt.Size = new System.Drawing.Size(69, 20);
            this.mkdDtEnt.TabIndex = 7;
            this.mkdDtEnt.ValidatingType = typeof(System.DateTime);
            // 
            // lblDataSaida
            // 
            this.lblDataSaida.AutoSize = true;
            this.lblDataSaida.Location = new System.Drawing.Point(204, 188);
            this.lblDataSaida.Name = "lblDataSaida";
            this.lblDataSaida.Size = new System.Drawing.Size(79, 13);
            this.lblDataSaida.TabIndex = 23;
            this.lblDataSaida.Text = "Data De Saída";
            // 
            // lblDataEntrada
            // 
            this.lblDataEntrada.AutoSize = true;
            this.lblDataEntrada.Location = new System.Drawing.Point(11, 188);
            this.lblDataEntrada.Name = "lblDataEntrada";
            this.lblDataEntrada.Size = new System.Drawing.Size(87, 13);
            this.lblDataEntrada.TabIndex = 21;
            this.lblDataEntrada.Text = "Data De Entrada";
            // 
            // txtFabricante
            // 
            this.txtFabricante.Location = new System.Drawing.Point(104, 159);
            this.txtFabricante.Name = "txtFabricante";
            this.txtFabricante.Size = new System.Drawing.Size(127, 20);
            this.txtFabricante.TabIndex = 5;
            // 
            // lblFabricante
            // 
            this.lblFabricante.AutoSize = true;
            this.lblFabricante.Location = new System.Drawing.Point(41, 162);
            this.lblFabricante.Name = "lblFabricante";
            this.lblFabricante.Size = new System.Drawing.Size(57, 13);
            this.lblFabricante.TabIndex = 19;
            this.lblFabricante.Text = "Fabricante";
            // 
            // mkdValidade
            // 
            this.mkdValidade.Location = new System.Drawing.Point(289, 159);
            this.mkdValidade.Mask = "00/00/0000";
            this.mkdValidade.Name = "mkdValidade";
            this.mkdValidade.Size = new System.Drawing.Size(69, 20);
            this.mkdValidade.TabIndex = 6;
            this.mkdValidade.ValidatingType = typeof(System.DateTime);
            // 
            // lblValidade
            // 
            this.lblValidade.AutoSize = true;
            this.lblValidade.Location = new System.Drawing.Point(235, 162);
            this.lblValidade.Name = "lblValidade";
            this.lblValidade.Size = new System.Drawing.Size(48, 13);
            this.lblValidade.TabIndex = 17;
            this.lblValidade.Text = "Validade";
            // 
            // txtQtd
            // 
            this.txtQtd.Location = new System.Drawing.Point(302, 133);
            this.txtQtd.Name = "txtQtd";
            this.txtQtd.Size = new System.Drawing.Size(56, 20);
            this.txtQtd.TabIndex = 4;
            this.txtQtd.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtQtd_KeyPress);
            // 
            // lblQtd
            // 
            this.lblQtd.AutoSize = true;
            this.lblQtd.Location = new System.Drawing.Point(234, 136);
            this.lblQtd.Name = "lblQtd";
            this.lblQtd.Size = new System.Drawing.Size(62, 13);
            this.lblQtd.TabIndex = 15;
            this.lblQtd.Text = "Quantidade";
            // 
            // txtNomeProduto
            // 
            this.txtNomeProduto.Location = new System.Drawing.Point(104, 133);
            this.txtNomeProduto.Name = "txtNomeProduto";
            this.txtNomeProduto.Size = new System.Drawing.Size(127, 20);
            this.txtNomeProduto.TabIndex = 3;
            this.txtNomeProduto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNomeProduto_KeyPress);
            // 
            // lblNomeProduto
            // 
            this.lblNomeProduto.AutoSize = true;
            this.lblNomeProduto.Location = new System.Drawing.Point(3, 136);
            this.lblNomeProduto.Name = "lblNomeProduto";
            this.lblNomeProduto.Size = new System.Drawing.Size(90, 13);
            this.lblNomeProduto.TabIndex = 13;
            this.lblNomeProduto.Text = "Nome do Produto";
            // 
            // cbTpSai
            // 
            this.cbTpSai.FormattingEnabled = true;
            this.cbTpSai.Items.AddRange(new object[] {
            "Sexta Básica",
            "Ação Social",
            "Outros"});
            this.cbTpSai.Location = new System.Drawing.Point(237, 47);
            this.cbTpSai.Name = "cbTpSai";
            this.cbTpSai.Size = new System.Drawing.Size(124, 21);
            this.cbTpSai.TabIndex = 1;
            this.cbTpSai.Visible = false;
            this.cbTpSai.Click += new System.EventHandler(this.cbTpSai_Click);
            // 
            // lblTpSaida
            // 
            this.lblTpSaida.AutoSize = true;
            this.lblTpSaida.Location = new System.Drawing.Point(154, 50);
            this.lblTpSaida.Name = "lblTpSaida";
            this.lblTpSaida.Size = new System.Drawing.Size(77, 13);
            this.lblTpSaida.TabIndex = 10;
            this.lblTpSaida.Text = "Tipo De Saída";
            this.lblTpSaida.Visible = false;
            // 
            // cbTpEnt
            // 
            this.cbTpEnt.FormattingEnabled = true;
            this.cbTpEnt.Items.AddRange(new object[] {
            "Oferta",
            "Alimento",
            "Roupas",
            "Outros"});
            this.cbTpEnt.Location = new System.Drawing.Point(237, 20);
            this.cbTpEnt.Name = "cbTpEnt";
            this.cbTpEnt.Size = new System.Drawing.Size(124, 21);
            this.cbTpEnt.TabIndex = 0;
            this.cbTpEnt.Visible = false;
            this.cbTpEnt.Click += new System.EventHandler(this.cbTpEnt_Click);
            // 
            // lblTpEntrada
            // 
            this.lblTpEntrada.AutoSize = true;
            this.lblTpEntrada.Location = new System.Drawing.Point(146, 23);
            this.lblTpEntrada.Name = "lblTpEntrada";
            this.lblTpEntrada.Size = new System.Drawing.Size(85, 13);
            this.lblTpEntrada.TabIndex = 11;
            this.lblTpEntrada.Text = "Tipo De Entrada";
            this.lblTpEntrada.Visible = false;
            // 
            // txtDescricao
            // 
            this.txtDescricao.Location = new System.Drawing.Point(3, 252);
            this.txtDescricao.Multiline = true;
            this.txtDescricao.Name = "txtDescricao";
            this.txtDescricao.Size = new System.Drawing.Size(358, 137);
            this.txtDescricao.TabIndex = 9;
            this.txtDescricao.Leave += new System.EventHandler(this.txtDescricao_Leave);
            // 
            // lblDescricao
            // 
            this.lblDescricao.AutoSize = true;
            this.lblDescricao.Location = new System.Drawing.Point(3, 236);
            this.lblDescricao.Name = "lblDescricao";
            this.lblDescricao.Size = new System.Drawing.Size(55, 13);
            this.lblDescricao.TabIndex = 8;
            this.lblDescricao.Text = "Descrição";
            // 
            // gbTipoLanca
            // 
            this.gbTipoLanca.Controls.Add(this.rbtnSaida);
            this.gbTipoLanca.Controls.Add(this.rbtnEntrada);
            this.gbTipoLanca.Location = new System.Drawing.Point(2, 2);
            this.gbTipoLanca.Name = "gbTipoLanca";
            this.gbTipoLanca.Size = new System.Drawing.Size(123, 73);
            this.gbTipoLanca.TabIndex = 0;
            this.gbTipoLanca.TabStop = false;
            this.gbTipoLanca.Text = "Tipo do Lançamento:";
            // 
            // rbtnSaida
            // 
            this.rbtnSaida.AutoSize = true;
            this.rbtnSaida.Location = new System.Drawing.Point(29, 45);
            this.rbtnSaida.Name = "rbtnSaida";
            this.rbtnSaida.Size = new System.Drawing.Size(54, 17);
            this.rbtnSaida.TabIndex = 1;
            this.rbtnSaida.TabStop = true;
            this.rbtnSaida.Text = "Saída";
            this.rbtnSaida.UseVisualStyleBackColor = true;
            this.rbtnSaida.CheckedChanged += new System.EventHandler(this.rbtnSaida_CheckedChanged);
            // 
            // rbtnEntrada
            // 
            this.rbtnEntrada.AutoSize = true;
            this.rbtnEntrada.Location = new System.Drawing.Point(29, 22);
            this.rbtnEntrada.Name = "rbtnEntrada";
            this.rbtnEntrada.Size = new System.Drawing.Size(62, 17);
            this.rbtnEntrada.TabIndex = 0;
            this.rbtnEntrada.TabStop = true;
            this.rbtnEntrada.Text = "Entrada";
            this.rbtnEntrada.UseVisualStyleBackColor = true;
            this.rbtnEntrada.CheckedChanged += new System.EventHandler(this.rbtnEntrada_CheckedChanged);
            // 
            // pnlBotoes
            // 
            this.pnlBotoes.Controls.Add(this.btnLancar);
            this.pnlBotoes.Controls.Add(this.btnSair);
            this.pnlBotoes.Location = new System.Drawing.Point(0, 464);
            this.pnlBotoes.Name = "pnlBotoes";
            this.pnlBotoes.Size = new System.Drawing.Size(758, 35);
            this.pnlBotoes.TabIndex = 2;
            // 
            // frmAcaoSocial
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(758, 501);
            this.ControlBox = false;
            this.Controls.Add(this.pnlLancamentos);
            this.Controls.Add(this.lblReceitasDespesas);
            this.Controls.Add(this.gdAcSoc);
            this.Controls.Add(this.pnlBotoes);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(300, 141);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmAcaoSocial";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Ação Social";
            this.Activated += new System.EventHandler(this.frmAcaoSocial_Activated);
            ((System.ComponentModel.ISupportInitialize)(this.gdAcSoc)).EndInit();
            this.pnlLancamentos.ResumeLayout(false);
            this.pnlLancamentos.PerformLayout();
            this.gbTipoLanca.ResumeLayout(false);
            this.gbTipoLanca.PerformLayout();
            this.pnlBotoes.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnLancar;
        private System.Windows.Forms.Button btnSair;
        private System.Windows.Forms.DataGridView gdAcSoc;
        private System.Windows.Forms.Label lblReceitasDespesas;
        private System.Windows.Forms.Panel pnlLancamentos;
        private System.Windows.Forms.GroupBox gbTipoLanca;
        private System.Windows.Forms.RadioButton rbtnSaida;
        private System.Windows.Forms.RadioButton rbtnEntrada;
        private System.Windows.Forms.TextBox txtDescricao;
        private System.Windows.Forms.Label lblDescricao;
        private System.Windows.Forms.ComboBox cbTpSai;
        private System.Windows.Forms.Label lblTpSaida;
        private System.Windows.Forms.ComboBox cbTpEnt;
        private System.Windows.Forms.Label lblTpEntrada;
        private System.Windows.Forms.Label lblNomeProduto;
        private System.Windows.Forms.Label lblValidade;
        private System.Windows.Forms.TextBox txtQtd;
        private System.Windows.Forms.Label lblQtd;
        private System.Windows.Forms.TextBox txtNomeProduto;
        private System.Windows.Forms.MaskedTextBox mkdValidade;
        private System.Windows.Forms.MaskedTextBox mkdDtSai;
        private System.Windows.Forms.MaskedTextBox mkdDtEnt;
        private System.Windows.Forms.Label lblDataSaida;
        private System.Windows.Forms.Label lblDataEntrada;
        private System.Windows.Forms.TextBox txtFabricante;
        private System.Windows.Forms.Label lblFabricante;
        private System.Windows.Forms.TextBox txtId;
        private System.Windows.Forms.Label lblId;
        private System.Windows.Forms.Panel pnlBotoes;
    }
}