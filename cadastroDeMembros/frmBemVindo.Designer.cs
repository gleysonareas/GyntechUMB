﻿namespace GyntechUMB
{
    partial class frmBemVindo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBemVindo));
            this.lblGyntechUMB = new System.Windows.Forms.Label();
            this.pnlMenu = new System.Windows.Forms.Panel();
            this.btnLista = new System.Windows.Forms.Button();
            this.btnHome = new System.Windows.Forms.Button();
            this.btnAcaoSocial = new System.Windows.Forms.Button();
            this.btnCadastro = new System.Windows.Forms.Button();
            this.btnFinanceiro = new System.Windows.Forms.Button();
            this.pnlMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblGyntechUMB
            // 
            this.lblGyntechUMB.AutoSize = true;
            this.lblGyntechUMB.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGyntechUMB.ForeColor = System.Drawing.Color.DarkCyan;
            this.lblGyntechUMB.Location = new System.Drawing.Point(12, 9);
            this.lblGyntechUMB.Name = "lblGyntechUMB";
            this.lblGyntechUMB.Size = new System.Drawing.Size(269, 46);
            this.lblGyntechUMB.TabIndex = 3;
            this.lblGyntechUMB.Text = "Gyntech UMB";
            // 
            // pnlMenu
            // 
            this.pnlMenu.Controls.Add(this.btnLista);
            this.pnlMenu.Controls.Add(this.btnHome);
            this.pnlMenu.Controls.Add(this.btnAcaoSocial);
            this.pnlMenu.Controls.Add(this.btnCadastro);
            this.pnlMenu.Controls.Add(this.btnFinanceiro);
            this.pnlMenu.Location = new System.Drawing.Point(12, 58);
            this.pnlMenu.Name = "pnlMenu";
            this.pnlMenu.Size = new System.Drawing.Size(734, 39);
            this.pnlMenu.TabIndex = 2;
            // 
            // btnLista
            // 
            this.btnLista.Location = new System.Drawing.Point(155, 3);
            this.btnLista.Name = "btnLista";
            this.btnLista.Size = new System.Drawing.Size(137, 32);
            this.btnLista.TabIndex = 1;
            this.btnLista.Text = "Lista De Membros / F8";
            this.btnLista.UseVisualStyleBackColor = true;
            this.btnLista.Click += new System.EventHandler(this.btnLista_Click);
            // 
            // btnHome
            // 
            this.btnHome.Location = new System.Drawing.Point(12, 3);
            this.btnHome.Name = "btnHome";
            this.btnHome.Size = new System.Drawing.Size(137, 32);
            this.btnHome.TabIndex = 0;
            this.btnHome.Text = "Home";
            this.btnHome.UseVisualStyleBackColor = true;
            this.btnHome.Click += new System.EventHandler(this.btnHome_Click);
            // 
            // btnAcaoSocial
            // 
            this.btnAcaoSocial.Location = new System.Drawing.Point(584, 3);
            this.btnAcaoSocial.Name = "btnAcaoSocial";
            this.btnAcaoSocial.Size = new System.Drawing.Size(137, 32);
            this.btnAcaoSocial.TabIndex = 4;
            this.btnAcaoSocial.Text = "Ação Social / F11";
            this.btnAcaoSocial.UseVisualStyleBackColor = true;
            this.btnAcaoSocial.Click += new System.EventHandler(this.btnAcaoSocial_Click);
            // 
            // btnCadastro
            // 
            this.btnCadastro.Location = new System.Drawing.Point(298, 3);
            this.btnCadastro.Name = "btnCadastro";
            this.btnCadastro.Size = new System.Drawing.Size(137, 32);
            this.btnCadastro.TabIndex = 2;
            this.btnCadastro.Text = "Cadastro / F9";
            this.btnCadastro.UseVisualStyleBackColor = true;
            this.btnCadastro.Click += new System.EventHandler(this.btnCadastro_Click);
            // 
            // btnFinanceiro
            // 
            this.btnFinanceiro.Location = new System.Drawing.Point(441, 3);
            this.btnFinanceiro.Name = "btnFinanceiro";
            this.btnFinanceiro.Size = new System.Drawing.Size(137, 32);
            this.btnFinanceiro.TabIndex = 3;
            this.btnFinanceiro.Text = "Financeiro / F10";
            this.btnFinanceiro.UseVisualStyleBackColor = true;
            this.btnFinanceiro.Click += new System.EventHandler(this.btnFinanceiro_Click);
            // 
            // frmBemVindo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(758, 110);
            this.Controls.Add(this.lblGyntechUMB);
            this.Controls.Add(this.pnlMenu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(300, 0);
            this.MaximizeBox = false;
            this.Name = "frmBemVindo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Bem Vindo";
            this.pnlMenu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblGyntechUMB;
        private System.Windows.Forms.Panel pnlMenu;
        private System.Windows.Forms.Button btnLista;
        private System.Windows.Forms.Button btnHome;
        private System.Windows.Forms.Button btnAcaoSocial;
        private System.Windows.Forms.Button btnCadastro;
        private System.Windows.Forms.Button btnFinanceiro;
    }
}