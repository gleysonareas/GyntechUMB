﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using iTextSharp.text.pdf;
using iTextSharp.text;
using Gyntech.DTO;
using Gyntech.Model;

namespace GyntechUMB
{
    public partial class frmCadastro : Form
    {
        #region Programa Principal

        MembroDTO mDTO = new MembroDTO();
        MembroModel mModel = new MembroModel();

        public frmCadastro()
        {
            InitializeComponent();
        }

        public int idMembroSelecionado;
        public string caminhoSelecionado;

        private void formularioCad_Load(object sender, EventArgs e)
        {
            if (idMembroSelecionado > 0)
                carregarDoBanco();
        }

        #region Lógica das caixas de texto
        //Nome Completo
        private void txtNomeCompleto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsLetter(e.KeyChar)) && (e.KeyChar != (char)8) && (e.KeyChar != (char)32))
                e.Handled = true;
        }

        //Nacionalidade
        private void txtNacionalidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsLetter(e.KeyChar)) && (e.KeyChar != (char)8) && (e.KeyChar != (char)32))
                e.Handled = true;
        }

        private void txtNacionalidade_Leave(object sender, EventArgs e)
        {
            if (txtNacionalidade.Text == "")
            {
                MessageBox.Show("O campo NACIONALIDADE necessita ser preenchido. se não houver nacionalidade definida, será preenchido com o valor: 'BRASILEIRO(A)'.");
                txtNacionalidade.Text = "Brasileiro(a)";
            }
        }

        //Naturalidade
        private void txtNatural_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsLetter(e.KeyChar)) && (e.KeyChar != (char)8) && (e.KeyChar != (char)32))
                e.Handled = true;
        }

        private void txtNatural_Leave(object sender, EventArgs e)
        {
            if (txtNatural.Text == "")
            {
                MessageBox.Show("O campo NATURALIDADE necessita ser preenchido. Caso não haja valores o preencheremos como 'NATURAL DE NOVA FRIBURGO'.");
                txtNatural.Text = "Nova Friburgo";
            }
        }

        //Estado Civil
        private void txtEstadoCivil_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsLetter(e.KeyChar)) && (e.KeyChar != (char)8) && (e.KeyChar != (char)32))
                e.Handled = true;
        }

        private void txtEstadoCivil_Leave(object sender, EventArgs e)
        {
            if (txtEstadoCivil.Text == "")
            {
                MessageBox.Show("O campo ESTADO CIVIL necessita ser preenchido. Se não houver valor a incerir será preenchido como Solteiro");
                txtEstadoCivil.Text = "Solteiro";
            }
        }

        //Função
        private void txtFuncao_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsLetter(e.KeyChar)) && (e.KeyChar != (char)8) && (e.KeyChar != (char)32))
                e.Handled = true;
        }

        private void txtFuncao_Leave(object sender, EventArgs e)
        {
            if (txtFuncao.Text == "")
            {
                MessageBox.Show("O campo FUNÇÃO necessita ser preenchido. se não houver Função especificada preencheremos com Valor: 'Membro'.");
                txtFuncao.Text = "Membro";
            }
        }

        //Nome do Pai
        private void txtNomePai_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsLetter(e.KeyChar)) && (e.KeyChar != (char)8) && (e.KeyChar != (char)32))
                e.Handled = true;
        }

        private void txtNomePai_Leave(object sender, EventArgs e)
        {
            if (txtNomePai.Text == "")
                txtNomePai.Text = null;
        }

        //Nome da Mãe
        private void txtNomeMae_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsLetter(e.KeyChar)) && (e.KeyChar != (char)8) && (e.KeyChar != (char)32))
                e.Handled = true;
        }

        private void txtNomeMae_Leave(object sender, EventArgs e)
        {
            if (txtNomeMae.Text == "")
                txtNomeMae.Text = null;
        }

        //Telefone
        private void mkdTelefone_Leave(object sender, EventArgs e)
        {
            mkdTelefone.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;
            if (mkdTelefone.Text == "")
                mkdTelefone.Text = null;
            mkdTelefone.TextMaskFormat = MaskFormat.IncludePromptAndLiterals;
        }

        //e-Mail
        private void txtMail_Leave(object sender, EventArgs e)
        {
            if (txtMail.Text == "")
                txtMail.Text = null;
        }

        //Cep
        private void mkdCep_Leave(object sender, EventArgs e)
        {
            localizarCep();
        }

        //Número
        private void txtNumero_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((char.IsLetter(e.KeyChar)) && (e.KeyChar != (char)8))
                e.Handled = true;
        }

        private void txtNumero_Leave(object sender, EventArgs e)
        {
            if (txtNumero.Text == "")
            {
                MessageBox.Show("O campo NÚMERO precisa ser preenchido. Se não houver número o campo será preenchido com valor: 'S/N'.");
                txtNumero.Text = "S/N";
            }
        }

        //Complemento
        private void txtComplemento_Leave(object sender, EventArgs e)
        {
            if (txtComplemento.Text == "")
                txtComplemento.Text = null;
        }

        //Estado ou UF
        private void txtUf_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsLetter(e.KeyChar)) && (e.KeyChar != (char)8) && (e.KeyChar != (char)32))
                e.Handled = true;
        }

        private void txtUf_Leave(object sender, EventArgs e)
        {
            if (txtUf.Text == "")
                MessageBox.Show("O campo UF necessita ser preenchido");
        }

        //Cidade
        private void txtCidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsLetter(e.KeyChar)) && (e.KeyChar != (char)8) && (e.KeyChar != (char)32))
                e.Handled = true;
        }

        private void txtCidade_Leave(object sender, EventArgs e)
        {
            if (txtCidade.Text == "")
                MessageBox.Show("O campo CIDADE necessita ser preenchido.");
        }

        //Bairro
        private void txtBairro_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsLetter(e.KeyChar)) && (e.KeyChar != (char)8) && (e.KeyChar != (char)32))
                e.Handled = true;
        }

        private void txtBairro_Leave(object sender, EventArgs e)
        {
            if (txtBairro.Text == "")
                MessageBox.Show("Este campo necessita ser preenchido.");
        }
        #endregion

        #region Lógica dos Botões
        //Picture Box imagem Perfil
        private void imgPerfil_DoubleClick(object sender, EventArgs e)
        {
            carregarImagem(); //ainda em desenvolvimento
        }

        //Botão de Radio
        private void rbtnInativo_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtnInativo.Checked == true)
            {
                desativarCampos();
            }
        }

        //Botão Salvar
        private void btnSalvarAtualizar_Click(object sender, EventArgs e)
        {
            List<string> mensagem = ValidarTela();

            if (mensagem.Count == 0)
            {
                salvarOuAtulizarNoBanco();
                fecharJanela();
            }
            else
            {
                string msg = "";
                foreach (var m in mensagem)
                {
                    msg = msg + Environment.NewLine + m;
                }
                MessageBox.Show(msg, "Erros no preenchimento", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        //Botão Imprimir /ainda em desenvolvimento
        private void btnImprimir_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fdb = new FolderBrowserDialog();
            if (fdb.ShowDialog() == DialogResult.OK)
            {
                caminhoSelecionado = fdb.SelectedPath;
            }

            if (validarDiretorio() == true)
            {
                List<string> mensagem = ValidarTela();

                if (mensagem.Count == 0)
                {
                    imprimirRelatorio();
                    MessageBox.Show("Ficha de Membro de {0}, Impresso com sucésso", txtNomeCompleto.Text);
                }
                else
                {
                    string msg = "";
                    foreach (var m in mensagem)
                    {
                        msg = msg + Environment.NewLine + m;
                    }
                    MessageBox.Show(msg, "Erros no preenchimento", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }

        }

        //Botão Cancelar
        private void btnCancel_Click(object sender, EventArgs e)
        {
            fecharJanela();
        }
        #endregion

        #endregion

        #region Funções
        //Função que carrega imagem
        private void carregarImagem() //ainda em desenvolvimento
        {
            /*string path = @"D:\Documentos\Gleyson\Trabalhos\C - Sharp\Desktop\cadastroDeMembros_ V2\cadastroDeMembros\bin\Debug\imagens";

            try
            {
                if (Directory.Exists(path))
                {
                    MessageBox.Show("O diretório já existe na localização atual");
                    return;
                }

                DirectoryInfo di = Directory.CreateDirectory(path);
                MessageBox.Show("O diretório foi criado com Sucesso. "+ Directory.GetCreationTime(path));
            }
            catch (Exception e)
            {
                MessageBox.Show("O processo falhou: {0}", e.ToString());
            }
            finally { }*/
        }

        //Função que valida o diretório do relatório
        private bool validarDiretorio()
        {
            string msg = "";

            if (caminhoSelecionado.Length <= 1)
            {
                msg += "- Selecione um diretório de saída";
            }

            if (msg == "")
            {
                return true;
            }
            else
            {
                MessageBox.Show(msg);
                return false;
            }
        }

        //Função que imprimi relatório em PDF
        private void imprimirRelatorio() //ainda em desenvolvimento
        {
            string caminho = string.Format("{0}/Ficha_de_Membro_de_{1}.pdf", caminhoSelecionado, txtNomeCompleto.Text);

            Document doc = new Document(PageSize.A4, 20, 20, 10, 10);
            FileStream fs = new FileStream(caminho, FileMode.Create, FileAccess.Write, FileShare.None);
            PdfWriter pdfWriter = PdfWriter.GetInstance(doc, fs);

            doc.Open();

            Paragraph pNomeCompleto = new Paragraph(string.Format("Ficha de membro de {0}", txtNomeCompleto.Text));
            doc.Add(pNomeCompleto);


            doc.Close();
        }

        //Função que realiza a verificação do STATUS DO MEMBRO
        private void desativarCampos()
        {
            txtNomeCompleto.Enabled = false;
            mkdDataNascimento.Enabled = false;
            mkdDataBatismo.Enabled = false;
            mkdRg.Enabled = false;
            mkdCpf.Enabled = false;
            txtNacionalidade.Enabled = false;
            txtNatural.Enabled = false;
            txtNomePai.Enabled = false;
            txtNomeMae.Enabled = false;
            txtEstadoCivil.Enabled = false;
            txtFuncao.Enabled = false;
            gbSexo.Enabled = false;
            gbRecebidoPor.Enabled = false;
            gbBatizado.Enabled = false;
            mkdTelefone.Enabled = false;
            mkdCelular.Enabled = false;
            txtMail.Enabled = false;
            mkdCep.Enabled = false;
            txtEndereco.Enabled = false;
            txtNumero.Enabled = false;
            txtComplemento.Enabled = false;
            txtUf.Enabled = false;
            txtCidade.Enabled = false;
            txtBairro.Enabled = false;
            btnSalvarAtualizar.Enabled = false;
            imgPerfil.Enabled = false;
        }

        //Função que realiza limpeza de campos com mascarás
        private string CleanFieldsMKD(string text)
        {
            string result = string.Empty;
            result = text.Replace("-", "").Replace("(", "").Replace(")", "").Replace(" ", "").Replace("_", "");
            return result;
        }

        //Função que realiza a validação dos campos
        private List<string> ValidarTela()
        {
            List<string> erros = new List<string>();

            //validação campo nome
            if (string.IsNullOrWhiteSpace(txtNomeCompleto.Text))
                erros.Add("Nome Completo inválido!");

            //validação campo nascimento
            DateTime dataNasc;
            if (DateTime.TryParse(mkdDataNascimento.Text, out dataNasc))
            {
                if (dataNasc.Year < 1900 || dataNasc.Year > DateTime.Today.Year)
                {
                    erros.Add("Data de nascimento inválida!");
                }
            }
            else
            {
                erros.Add("Data de nascimento inválida!");
            }

            //validação campo batismo
            DateTime dataBatism;
            if (DateTime.TryParse(mkdDataBatismo.Text, out dataBatism))
            {
                if (dataBatism.Year < 1900 || dataBatism.Year > DateTime.Today.Year)
                {
                    erros.Add("Data de batismo inválida!");
                }
            }

            else
            {
                erros.Add("Data de batismo inválida!");
            }

            //validação rg
            mkdRg.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;
            if (string.IsNullOrWhiteSpace(mkdRg.Text))
            {
                erros.Add("RG inválido!");
            }
            mkdRg.TextMaskFormat = MaskFormat.IncludePromptAndLiterals;

            //validação cpf
            mkdCpf.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;
            if (string.IsNullOrWhiteSpace(mkdCpf.Text))
            {
                erros.Add("CPF inválido");
            }
            mkdCpf.TextMaskFormat = MaskFormat.IncludePromptAndLiterals;

            //validação nacionalidade
            if (string.IsNullOrWhiteSpace(txtNacionalidade.Text))
                erros.Add("Nacionalidade inválida!");

            //validação naturalidade
            if (string.IsNullOrWhiteSpace(txtNatural.Text))
                erros.Add("Naturalidade inválida!");

            //validação estado civil
            if (string.IsNullOrWhiteSpace(txtEstadoCivil.Text))
                erros.Add("Estado civil inválido!");

            //validação função desempenhada
            if (string.IsNullOrWhiteSpace(txtFuncao.Text))
                erros.Add("Função desempenhada inválida!");

            //validação celular
            mkdCelular.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;
            if (string.IsNullOrWhiteSpace(mkdCelular.Text))
            {
                erros.Add("Celular inválido");
            }
            mkdCelular.TextMaskFormat = MaskFormat.IncludePromptAndLiterals;

            //validação cep
            mkdCep.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;
            if (string.IsNullOrWhiteSpace(mkdCep.Text))
            {
                erros.Add("CEP inválido");
            }
            mkdCep.TextMaskFormat = MaskFormat.IncludePromptAndLiterals;

            //validação endereço
            if (string.IsNullOrWhiteSpace(txtEndereco.Text))
                erros.Add("Endereço inválido!");

            //validação número
            if (string.IsNullOrWhiteSpace(txtNumero.Text))
                erros.Add("Número inválido");

            //validação uf
            if (string.IsNullOrWhiteSpace(txtUf.Text))
                erros.Add("UF inválido!");

            //validação cidade
            if (string.IsNullOrWhiteSpace(txtCidade.Text))
                erros.Add("Cidade inválida!");

            //validação bairro
            if (string.IsNullOrWhiteSpace(txtBairro.Text))
                erros.Add("Bairro inválido!");

            return erros;
        }

        //Função que localiza o endereço pelo Cep
        private void localizarCep()
        {
            mkdCep.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;
            using (var ws = new WSCorreios.AtendeClienteClient())
            {
                try
                {
                    var resultado = ws.consultaCEP(mkdCep.Text);
                    txtEndereco.Text = resultado.end;
                    txtComplemento.Text = resultado.complemento;
                    txtCidade.Text = resultado.cidade;
                    txtBairro.Text = resultado.bairro;
                    txtUf.Text = resultado.uf;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            mkdCep.TextMaskFormat = MaskFormat.IncludePromptAndLiterals;
        }

        //Função Salvar no banco
        private void salvarOuAtulizarNoBanco()
        {
            try
            {
                mDTO.NomeCompleto = txtNomeCompleto.Text;
                mDTO.Nascimento = Convert.ToDateTime(mkdDataNascimento.Text);
                mDTO.Batismo = Convert.ToDateTime(mkdDataBatismo.Text);
                mDTO.Rg = mkdRg.Text;
                mDTO.Cpf = mkdCpf.Text;


                mDTO.Nacionalidade = txtNacionalidade.Text;
                mDTO.Naturalidade = txtNatural.Text;
                mDTO.NomePai = txtNomePai.Text;
                mDTO.NomeMae = txtNomeMae.Text;
                mDTO.EstadoCivil = txtEstadoCivil.Text;
                mDTO.Funcao = txtFuncao.Text;

                //Sexo
                if (rbtnMasculino.Checked == true)
                    mDTO.Sexo = "Masculino";
                else
                    mDTO.Sexo = "Feminino";

                //Recebido
                if (rbtnBatismo.Checked == true)
                    mDTO.RecebidoPor = "Batismo";
                else if (rbtnAdesao.Checked == true)
                    mDTO.RecebidoPor = "Adesão";
                else
                    mDTO.RecebidoPor = "Transferência";

                //Batizado
                if (rbtnSim.Checked == true)
                    mDTO.Batizado = "Sim";
                else
                    mDTO.Batizado = "Não";

                //Status
                if (rbtnAtivo.Checked == true)
                    mDTO.Status = "Ativo";
                else
                    mDTO.Status = "Inativo";

                mDTO.Telefone = mkdTelefone.Text;
                mDTO.Celular = mkdCelular.Text;
                mDTO.EMail = txtMail.Text;
                mDTO.Cep = mkdCep.Text;
                mDTO.Endereco = txtEndereco.Text;
                mDTO.Numero = txtNumero.Text;
                mDTO.Complemento = txtComplemento.Text;
                mDTO.Uf = txtUf.Text;
                mDTO.Cidade = txtCidade.Text;
                mDTO.Bairro = txtBairro.Text;
                
                if (idMembroSelecionado == 0)
                {
                    int mb = mModel.Inserir(mDTO);
                    if (mb > 0)
                        MessageBox.Show("O cadastro foi realizado com sucesso");
                }
                else
                {
                    mDTO.Id = idMembroSelecionado;
                    int mb = mModel.Atulizar(mDTO);
                    if (mb > 0)
                        MessageBox.Show("O cadastro foi alterado com sucesso");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ocorreu um erro ao salvar o novo cadastro ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Função Carregar do banco
        private void carregarDoBanco()
        {
            try
            {
                IList<MembroDTO> membroSelecionado = new List<MembroDTO>();
                membroSelecionado = mModel.CarregarDoBanco(idMembroSelecionado);

                foreach (var item in membroSelecionado)
                {
                    txtId.Text = Convert.ToString(item.Id);
                    txtNomeCompleto.Text = item.NomeCompleto;
                    mkdDataNascimento.Text = Convert.ToString(item.Nascimento);
                    mkdDataBatismo.Text = Convert.ToString(item.Batismo);
                    mkdRg.Text = item.Rg;
                    mkdCpf.Text = item.Cpf;
                    txtNomePai.Text = item.NomePai;
                    txtNomeMae.Text = item.NomeMae;
                    txtNacionalidade.Text = item.Nacionalidade;
                    txtNatural.Text = item.Naturalidade;
                    txtEstadoCivil.Text = item.EstadoCivil;
                    txtFuncao.Text = item.Funcao;

                    //Sexo
                    if (item.Sexo == "Masculino")
                    {
                        rbtnMasculino.Checked = true;
                        rbtnFeminino.Checked = false;
                    }
                    else
                    {
                        rbtnMasculino.Checked = false;
                        rbtnFeminino.Checked = true;
                    }

                    //Recebido
                    if (item.RecebidoPor == "Batismo")
                    {
                        rbtnBatismo.Checked = true;
                        rbtnAdesao.Checked = false;
                        rbtnTransferencia.Checked = false;
                    }
                    else if (item.RecebidoPor == "Adesão")
                    {
                        rbtnBatismo.Checked = false;
                        rbtnAdesao.Checked = true;
                        rbtnTransferencia.Checked = false;
                    }
                    else
                    {
                        rbtnBatismo.Checked = false;
                        rbtnAdesao.Checked = false;
                        rbtnTransferencia.Checked = true;
                    }

                    //Batizado
                    if (item.Batizado == "Sim")
                    {
                        rbtnSim.Checked = true;
                        rbtnNao.Checked = false;
                    }
                    else
                    {
                        rbtnSim.Checked = false;
                        rbtnNao.Checked = true;
                    }

                    //Status
                    if (item.Status == "Ativo")
                    {
                        rbtnAtivo.Checked = true;
                        rbtnInativo.Checked = false;
                    }
                    else
                    {
                        rbtnAtivo.Checked = false;
                        rbtnInativo.Checked = true;
                    }

                    mkdTelefone.Text = item.Telefone;
                    mkdCelular.Text = item.Celular;
                    txtMail.Text = item.EMail;
                    mkdCep.Text = item.Cep;
                    txtEndereco.Text = item.Endereco;
                    txtNumero.Text = item.Numero;
                    txtComplemento.Text = item.Complemento;
                    txtUf.Text = item.Uf;
                    txtCidade.Text = item.Cidade;
                    txtBairro.Text = item.Bairro;
                }
               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ocorreu um erro ao carregar os dados para edição", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        //Função que volta para FORM anterior
        private void fecharJanela()
        {
            this.Close();
        }
        #endregion
    }
}