﻿namespace GyntechUMB
{
    partial class frmCadastro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCadastro));
            this.pnlMenuBaixo = new System.Windows.Forms.Panel();
            this.btnImprimir = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSalvarAtualizar = new System.Windows.Forms.Button();
            this.pnlLocalizacao = new System.Windows.Forms.Panel();
            this.lblLocaliza = new System.Windows.Forms.Label();
            this.txtBairro = new System.Windows.Forms.TextBox();
            this.lblBairro = new System.Windows.Forms.Label();
            this.txtCidade = new System.Windows.Forms.TextBox();
            this.lblCidade = new System.Windows.Forms.Label();
            this.txtUf = new System.Windows.Forms.TextBox();
            this.lblUf = new System.Windows.Forms.Label();
            this.txtComplemento = new System.Windows.Forms.TextBox();
            this.lblComplemento = new System.Windows.Forms.Label();
            this.txtNumero = new System.Windows.Forms.TextBox();
            this.lblNumero = new System.Windows.Forms.Label();
            this.txtEndereco = new System.Windows.Forms.TextBox();
            this.lblEndereco = new System.Windows.Forms.Label();
            this.mkdCep = new System.Windows.Forms.MaskedTextBox();
            this.lblCep = new System.Windows.Forms.Label();
            this.pnlContatos = new System.Windows.Forms.Panel();
            this.lblContatos = new System.Windows.Forms.Label();
            this.txtMail = new System.Windows.Forms.TextBox();
            this.lblMail = new System.Windows.Forms.Label();
            this.mkdCelular = new System.Windows.Forms.MaskedTextBox();
            this.lblCelular = new System.Windows.Forms.Label();
            this.mkdTelefone = new System.Windows.Forms.MaskedTextBox();
            this.lblTelefone = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnlOutrosDados = new System.Windows.Forms.Panel();
            this.gbStatus = new System.Windows.Forms.GroupBox();
            this.rbtnInativo = new System.Windows.Forms.RadioButton();
            this.rbtnAtivo = new System.Windows.Forms.RadioButton();
            this.gbBatizado = new System.Windows.Forms.GroupBox();
            this.rbtnNao = new System.Windows.Forms.RadioButton();
            this.rbtnSim = new System.Windows.Forms.RadioButton();
            this.gbRecebidoPor = new System.Windows.Forms.GroupBox();
            this.rbtnTransferencia = new System.Windows.Forms.RadioButton();
            this.rbtnAdesao = new System.Windows.Forms.RadioButton();
            this.rbtnBatismo = new System.Windows.Forms.RadioButton();
            this.gbSexo = new System.Windows.Forms.GroupBox();
            this.rbtnMasculino = new System.Windows.Forms.RadioButton();
            this.rbtnFeminino = new System.Windows.Forms.RadioButton();
            this.lblOutrosDados = new System.Windows.Forms.Label();
            this.pnlIdentificacao = new System.Windows.Forms.Panel();
            this.txtId = new System.Windows.Forms.TextBox();
            this.lblId = new System.Windows.Forms.Label();
            this.txtNacionalidade = new System.Windows.Forms.TextBox();
            this.lblNacionalidade = new System.Windows.Forms.Label();
            this.lblIdentifica = new System.Windows.Forms.Label();
            this.imgPerfil = new System.Windows.Forms.PictureBox();
            this.txtFuncao = new System.Windows.Forms.TextBox();
            this.txtEstadoCivil = new System.Windows.Forms.TextBox();
            this.txtNatural = new System.Windows.Forms.TextBox();
            this.lblFuncao = new System.Windows.Forms.Label();
            this.lblEstadoCivil = new System.Windows.Forms.Label();
            this.lblNatural = new System.Windows.Forms.Label();
            this.mkdDataBatismo = new System.Windows.Forms.MaskedTextBox();
            this.lblDataBatismo = new System.Windows.Forms.Label();
            this.mkdCpf = new System.Windows.Forms.MaskedTextBox();
            this.lblCpf = new System.Windows.Forms.Label();
            this.mkdRg = new System.Windows.Forms.MaskedTextBox();
            this.lblRg = new System.Windows.Forms.Label();
            this.txtNomeMae = new System.Windows.Forms.TextBox();
            this.txtNomePai = new System.Windows.Forms.TextBox();
            this.lblNomeMae = new System.Windows.Forms.Label();
            this.lblNomePai = new System.Windows.Forms.Label();
            this.mkdDataNascimento = new System.Windows.Forms.MaskedTextBox();
            this.lblDataNascimento = new System.Windows.Forms.Label();
            this.txtNomeCompleto = new System.Windows.Forms.TextBox();
            this.lblNome = new System.Windows.Forms.Label();
            this.pnlMenuBaixo.SuspendLayout();
            this.pnlLocalizacao.SuspendLayout();
            this.pnlContatos.SuspendLayout();
            this.panel1.SuspendLayout();
            this.pnlOutrosDados.SuspendLayout();
            this.gbStatus.SuspendLayout();
            this.gbBatizado.SuspendLayout();
            this.gbRecebidoPor.SuspendLayout();
            this.gbSexo.SuspendLayout();
            this.pnlIdentificacao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgPerfil)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlMenuBaixo
            // 
            this.pnlMenuBaixo.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.pnlMenuBaixo.Controls.Add(this.btnImprimir);
            this.pnlMenuBaixo.Controls.Add(this.btnCancel);
            this.pnlMenuBaixo.Controls.Add(this.btnSalvarAtualizar);
            this.pnlMenuBaixo.Location = new System.Drawing.Point(0, 466);
            this.pnlMenuBaixo.Name = "pnlMenuBaixo";
            this.pnlMenuBaixo.Size = new System.Drawing.Size(758, 35);
            this.pnlMenuBaixo.TabIndex = 5;
            // 
            // btnImprimir
            // 
            this.btnImprimir.Location = new System.Drawing.Point(315, 0);
            this.btnImprimir.Name = "btnImprimir";
            this.btnImprimir.Size = new System.Drawing.Size(137, 32);
            this.btnImprimir.TabIndex = 2;
            this.btnImprimir.Text = "Imprimir";
            this.btnImprimir.UseVisualStyleBackColor = true;
            this.btnImprimir.Click += new System.EventHandler(this.btnImprimir_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnCancel.Location = new System.Drawing.Point(458, 0);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(137, 32);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Cancelar";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSalvarAtualizar
            // 
            this.btnSalvarAtualizar.Location = new System.Drawing.Point(172, 0);
            this.btnSalvarAtualizar.Name = "btnSalvarAtualizar";
            this.btnSalvarAtualizar.Size = new System.Drawing.Size(137, 32);
            this.btnSalvarAtualizar.TabIndex = 1;
            this.btnSalvarAtualizar.Text = "Salvar/Atualizar";
            this.btnSalvarAtualizar.UseVisualStyleBackColor = true;
            this.btnSalvarAtualizar.Click += new System.EventHandler(this.btnSalvarAtualizar_Click);
            // 
            // pnlLocalizacao
            // 
            this.pnlLocalizacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlLocalizacao.Controls.Add(this.lblLocaliza);
            this.pnlLocalizacao.Controls.Add(this.txtBairro);
            this.pnlLocalizacao.Controls.Add(this.lblBairro);
            this.pnlLocalizacao.Controls.Add(this.txtCidade);
            this.pnlLocalizacao.Controls.Add(this.lblCidade);
            this.pnlLocalizacao.Controls.Add(this.txtUf);
            this.pnlLocalizacao.Controls.Add(this.lblUf);
            this.pnlLocalizacao.Controls.Add(this.txtComplemento);
            this.pnlLocalizacao.Controls.Add(this.lblComplemento);
            this.pnlLocalizacao.Controls.Add(this.txtNumero);
            this.pnlLocalizacao.Controls.Add(this.lblNumero);
            this.pnlLocalizacao.Controls.Add(this.txtEndereco);
            this.pnlLocalizacao.Controls.Add(this.lblEndereco);
            this.pnlLocalizacao.Controls.Add(this.mkdCep);
            this.pnlLocalizacao.Controls.Add(this.lblCep);
            this.pnlLocalizacao.Location = new System.Drawing.Point(12, 339);
            this.pnlLocalizacao.Name = "pnlLocalizacao";
            this.pnlLocalizacao.Size = new System.Drawing.Size(738, 116);
            this.pnlLocalizacao.TabIndex = 4;
            // 
            // lblLocaliza
            // 
            this.lblLocaliza.AutoSize = true;
            this.lblLocaliza.Location = new System.Drawing.Point(-1, -1);
            this.lblLocaliza.Name = "lblLocaliza";
            this.lblLocaliza.Size = new System.Drawing.Size(67, 13);
            this.lblLocaliza.TabIndex = 131;
            this.lblLocaliza.Text = "Localização:";
            // 
            // txtBairro
            // 
            this.txtBairro.Location = new System.Drawing.Point(351, 81);
            this.txtBairro.Name = "txtBairro";
            this.txtBairro.Size = new System.Drawing.Size(186, 20);
            this.txtBairro.TabIndex = 7;
            this.txtBairro.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBairro_KeyPress);
            this.txtBairro.Leave += new System.EventHandler(this.txtBairro_Leave);
            // 
            // lblBairro
            // 
            this.lblBairro.AutoSize = true;
            this.lblBairro.Location = new System.Drawing.Point(311, 84);
            this.lblBairro.Name = "lblBairro";
            this.lblBairro.Size = new System.Drawing.Size(34, 13);
            this.lblBairro.TabIndex = 129;
            this.lblBairro.Text = "Bairro";
            // 
            // txtCidade
            // 
            this.txtCidade.Location = new System.Drawing.Point(120, 81);
            this.txtCidade.Name = "txtCidade";
            this.txtCidade.Size = new System.Drawing.Size(185, 20);
            this.txtCidade.TabIndex = 6;
            this.txtCidade.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCidade_KeyPress);
            this.txtCidade.Leave += new System.EventHandler(this.txtCidade_Leave);
            // 
            // lblCidade
            // 
            this.lblCidade.AutoSize = true;
            this.lblCidade.Location = new System.Drawing.Point(74, 84);
            this.lblCidade.Name = "lblCidade";
            this.lblCidade.Size = new System.Drawing.Size(40, 13);
            this.lblCidade.TabIndex = 127;
            this.lblCidade.Text = "Cidade";
            // 
            // txtUf
            // 
            this.txtUf.Location = new System.Drawing.Point(593, 55);
            this.txtUf.Name = "txtUf";
            this.txtUf.Size = new System.Drawing.Size(50, 20);
            this.txtUf.TabIndex = 5;
            this.txtUf.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtUf_KeyPress);
            this.txtUf.Leave += new System.EventHandler(this.txtUf_Leave);
            // 
            // lblUf
            // 
            this.lblUf.AutoSize = true;
            this.lblUf.Location = new System.Drawing.Point(566, 58);
            this.lblUf.Name = "lblUf";
            this.lblUf.Size = new System.Drawing.Size(21, 13);
            this.lblUf.TabIndex = 125;
            this.lblUf.Text = "UF";
            // 
            // txtComplemento
            // 
            this.txtComplemento.Location = new System.Drawing.Point(120, 55);
            this.txtComplemento.Name = "txtComplemento";
            this.txtComplemento.Size = new System.Drawing.Size(417, 20);
            this.txtComplemento.TabIndex = 4;
            this.txtComplemento.Leave += new System.EventHandler(this.txtComplemento_Leave);
            // 
            // lblComplemento
            // 
            this.lblComplemento.AutoSize = true;
            this.lblComplemento.Location = new System.Drawing.Point(43, 58);
            this.lblComplemento.Name = "lblComplemento";
            this.lblComplemento.Size = new System.Drawing.Size(71, 13);
            this.lblComplemento.TabIndex = 123;
            this.lblComplemento.Text = "Complemento";
            // 
            // txtNumero
            // 
            this.txtNumero.Location = new System.Drawing.Point(593, 29);
            this.txtNumero.Name = "txtNumero";
            this.txtNumero.Size = new System.Drawing.Size(50, 20);
            this.txtNumero.TabIndex = 3;
            this.txtNumero.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNumero_KeyPress);
            this.txtNumero.Leave += new System.EventHandler(this.txtNumero_Leave);
            // 
            // lblNumero
            // 
            this.lblNumero.AutoSize = true;
            this.lblNumero.Location = new System.Drawing.Point(543, 32);
            this.lblNumero.Name = "lblNumero";
            this.lblNumero.Size = new System.Drawing.Size(44, 13);
            this.lblNumero.TabIndex = 121;
            this.lblNumero.Text = "Número";
            // 
            // txtEndereco
            // 
            this.txtEndereco.Location = new System.Drawing.Point(120, 29);
            this.txtEndereco.Name = "txtEndereco";
            this.txtEndereco.Size = new System.Drawing.Size(417, 20);
            this.txtEndereco.TabIndex = 2;
            // 
            // lblEndereco
            // 
            this.lblEndereco.AutoSize = true;
            this.lblEndereco.Location = new System.Drawing.Point(61, 32);
            this.lblEndereco.Name = "lblEndereco";
            this.lblEndereco.Size = new System.Drawing.Size(53, 13);
            this.lblEndereco.TabIndex = 119;
            this.lblEndereco.Text = "Endereço";
            // 
            // mkdCep
            // 
            this.mkdCep.Location = new System.Drawing.Point(120, 3);
            this.mkdCep.Mask = "00000-999";
            this.mkdCep.Name = "mkdCep";
            this.mkdCep.Size = new System.Drawing.Size(70, 20);
            this.mkdCep.TabIndex = 1;
            this.mkdCep.Leave += new System.EventHandler(this.mkdCep_Leave);
            // 
            // lblCep
            // 
            this.lblCep.AutoSize = true;
            this.lblCep.Location = new System.Drawing.Point(88, 6);
            this.lblCep.Name = "lblCep";
            this.lblCep.Size = new System.Drawing.Size(26, 13);
            this.lblCep.TabIndex = 117;
            this.lblCep.Text = "Cep";
            // 
            // pnlContatos
            // 
            this.pnlContatos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlContatos.Controls.Add(this.lblContatos);
            this.pnlContatos.Controls.Add(this.txtMail);
            this.pnlContatos.Controls.Add(this.lblMail);
            this.pnlContatos.Controls.Add(this.mkdCelular);
            this.pnlContatos.Controls.Add(this.lblCelular);
            this.pnlContatos.Controls.Add(this.mkdTelefone);
            this.pnlContatos.Controls.Add(this.lblTelefone);
            this.pnlContatos.Location = new System.Drawing.Point(12, 258);
            this.pnlContatos.Name = "pnlContatos";
            this.pnlContatos.Size = new System.Drawing.Size(738, 82);
            this.pnlContatos.TabIndex = 3;
            // 
            // lblContatos
            // 
            this.lblContatos.AutoSize = true;
            this.lblContatos.Location = new System.Drawing.Point(-1, -1);
            this.lblContatos.Name = "lblContatos";
            this.lblContatos.Size = new System.Drawing.Size(52, 13);
            this.lblContatos.TabIndex = 125;
            this.lblContatos.Text = "Contatos:";
            // 
            // txtMail
            // 
            this.txtMail.Location = new System.Drawing.Point(120, 55);
            this.txtMail.Name = "txtMail";
            this.txtMail.Size = new System.Drawing.Size(221, 20);
            this.txtMail.TabIndex = 3;
            this.txtMail.Leave += new System.EventHandler(this.txtMail_Leave);
            // 
            // lblMail
            // 
            this.lblMail.AutoSize = true;
            this.lblMail.Location = new System.Drawing.Point(80, 58);
            this.lblMail.Name = "lblMail";
            this.lblMail.Size = new System.Drawing.Size(34, 13);
            this.lblMail.TabIndex = 123;
            this.lblMail.Text = "e-mail";
            // 
            // mkdCelular
            // 
            this.mkdCelular.Location = new System.Drawing.Point(120, 29);
            this.mkdCelular.Mask = "(00) 00000-0000";
            this.mkdCelular.Name = "mkdCelular";
            this.mkdCelular.Size = new System.Drawing.Size(100, 20);
            this.mkdCelular.TabIndex = 2;
            // 
            // lblCelular
            // 
            this.lblCelular.AutoSize = true;
            this.lblCelular.Location = new System.Drawing.Point(75, 32);
            this.lblCelular.Name = "lblCelular";
            this.lblCelular.Size = new System.Drawing.Size(39, 13);
            this.lblCelular.TabIndex = 121;
            this.lblCelular.Text = "Celular";
            // 
            // mkdTelefone
            // 
            this.mkdTelefone.Location = new System.Drawing.Point(120, 3);
            this.mkdTelefone.Mask = "(00) 0000-0000";
            this.mkdTelefone.Name = "mkdTelefone";
            this.mkdTelefone.Size = new System.Drawing.Size(95, 20);
            this.mkdTelefone.TabIndex = 1;
            this.mkdTelefone.Leave += new System.EventHandler(this.mkdTelefone_Leave);
            // 
            // lblTelefone
            // 
            this.lblTelefone.AutoSize = true;
            this.lblTelefone.Location = new System.Drawing.Point(65, 6);
            this.lblTelefone.Name = "lblTelefone";
            this.lblTelefone.Size = new System.Drawing.Size(49, 13);
            this.lblTelefone.TabIndex = 119;
            this.lblTelefone.Text = "Telefone";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panel1.Controls.Add(this.pnlOutrosDados);
            this.panel1.Controls.Add(this.pnlIdentificacao);
            this.panel1.Controls.Add(this.pnlContatos);
            this.panel1.Controls.Add(this.pnlLocalizacao);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(758, 466);
            this.panel1.TabIndex = 0;
            // 
            // pnlOutrosDados
            // 
            this.pnlOutrosDados.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlOutrosDados.Controls.Add(this.gbStatus);
            this.pnlOutrosDados.Controls.Add(this.gbBatizado);
            this.pnlOutrosDados.Controls.Add(this.gbRecebidoPor);
            this.pnlOutrosDados.Controls.Add(this.gbSexo);
            this.pnlOutrosDados.Controls.Add(this.lblOutrosDados);
            this.pnlOutrosDados.Location = new System.Drawing.Point(12, 155);
            this.pnlOutrosDados.Name = "pnlOutrosDados";
            this.pnlOutrosDados.Size = new System.Drawing.Size(738, 104);
            this.pnlOutrosDados.TabIndex = 2;
            // 
            // gbStatus
            // 
            this.gbStatus.Controls.Add(this.rbtnInativo);
            this.gbStatus.Controls.Add(this.rbtnAtivo);
            this.gbStatus.Enabled = false;
            this.gbStatus.Location = new System.Drawing.Point(558, 3);
            this.gbStatus.Name = "gbStatus";
            this.gbStatus.Size = new System.Drawing.Size(135, 90);
            this.gbStatus.TabIndex = 15;
            this.gbStatus.TabStop = false;
            this.gbStatus.Text = "Status:";
            // 
            // rbtnInativo
            // 
            this.rbtnInativo.AutoSize = true;
            this.rbtnInativo.Location = new System.Drawing.Point(44, 50);
            this.rbtnInativo.Name = "rbtnInativo";
            this.rbtnInativo.Size = new System.Drawing.Size(57, 17);
            this.rbtnInativo.TabIndex = 9;
            this.rbtnInativo.Text = "Inativo";
            this.rbtnInativo.UseVisualStyleBackColor = true;
            this.rbtnInativo.CheckedChanged += new System.EventHandler(this.rbtnInativo_CheckedChanged);
            // 
            // rbtnAtivo
            // 
            this.rbtnAtivo.AutoSize = true;
            this.rbtnAtivo.Checked = true;
            this.rbtnAtivo.Location = new System.Drawing.Point(44, 27);
            this.rbtnAtivo.Name = "rbtnAtivo";
            this.rbtnAtivo.Size = new System.Drawing.Size(49, 17);
            this.rbtnAtivo.TabIndex = 8;
            this.rbtnAtivo.TabStop = true;
            this.rbtnAtivo.Text = "Ativo";
            this.rbtnAtivo.UseVisualStyleBackColor = true;
            // 
            // gbBatizado
            // 
            this.gbBatizado.Controls.Add(this.rbtnNao);
            this.gbBatizado.Controls.Add(this.rbtnSim);
            this.gbBatizado.Location = new System.Drawing.Point(431, 6);
            this.gbBatizado.Name = "gbBatizado";
            this.gbBatizado.Size = new System.Drawing.Size(106, 90);
            this.gbBatizado.TabIndex = 14;
            this.gbBatizado.TabStop = false;
            this.gbBatizado.Text = "Batizado:";
            // 
            // rbtnNao
            // 
            this.rbtnNao.AutoSize = true;
            this.rbtnNao.Location = new System.Drawing.Point(58, 47);
            this.rbtnNao.Name = "rbtnNao";
            this.rbtnNao.Size = new System.Drawing.Size(45, 17);
            this.rbtnNao.TabIndex = 7;
            this.rbtnNao.TabStop = true;
            this.rbtnNao.Text = "Não";
            this.rbtnNao.UseVisualStyleBackColor = true;
            // 
            // rbtnSim
            // 
            this.rbtnSim.AutoSize = true;
            this.rbtnSim.Checked = true;
            this.rbtnSim.Location = new System.Drawing.Point(58, 24);
            this.rbtnSim.Name = "rbtnSim";
            this.rbtnSim.Size = new System.Drawing.Size(42, 17);
            this.rbtnSim.TabIndex = 6;
            this.rbtnSim.TabStop = true;
            this.rbtnSim.Text = "Sim";
            this.rbtnSim.UseVisualStyleBackColor = true;
            // 
            // gbRecebidoPor
            // 
            this.gbRecebidoPor.Controls.Add(this.rbtnTransferencia);
            this.gbRecebidoPor.Controls.Add(this.rbtnAdesao);
            this.gbRecebidoPor.Controls.Add(this.rbtnBatismo);
            this.gbRecebidoPor.Location = new System.Drawing.Point(276, 6);
            this.gbRecebidoPor.Name = "gbRecebidoPor";
            this.gbRecebidoPor.Size = new System.Drawing.Size(130, 90);
            this.gbRecebidoPor.TabIndex = 13;
            this.gbRecebidoPor.TabStop = false;
            this.gbRecebidoPor.Text = "Recebido Por:";
            // 
            // rbtnTransferencia
            // 
            this.rbtnTransferencia.AutoSize = true;
            this.rbtnTransferencia.Location = new System.Drawing.Point(34, 70);
            this.rbtnTransferencia.Name = "rbtnTransferencia";
            this.rbtnTransferencia.Size = new System.Drawing.Size(90, 17);
            this.rbtnTransferencia.TabIndex = 5;
            this.rbtnTransferencia.TabStop = true;
            this.rbtnTransferencia.Text = "Transferência";
            this.rbtnTransferencia.UseVisualStyleBackColor = true;
            // 
            // rbtnAdesao
            // 
            this.rbtnAdesao.AutoSize = true;
            this.rbtnAdesao.Checked = true;
            this.rbtnAdesao.Location = new System.Drawing.Point(34, 47);
            this.rbtnAdesao.Name = "rbtnAdesao";
            this.rbtnAdesao.Size = new System.Drawing.Size(61, 17);
            this.rbtnAdesao.TabIndex = 4;
            this.rbtnAdesao.TabStop = true;
            this.rbtnAdesao.Text = "Adesão";
            this.rbtnAdesao.UseVisualStyleBackColor = true;
            // 
            // rbtnBatismo
            // 
            this.rbtnBatismo.AutoSize = true;
            this.rbtnBatismo.Location = new System.Drawing.Point(34, 24);
            this.rbtnBatismo.Name = "rbtnBatismo";
            this.rbtnBatismo.Size = new System.Drawing.Size(62, 17);
            this.rbtnBatismo.TabIndex = 3;
            this.rbtnBatismo.TabStop = true;
            this.rbtnBatismo.Text = "Batismo";
            this.rbtnBatismo.UseVisualStyleBackColor = true;
            // 
            // gbSexo
            // 
            this.gbSexo.Controls.Add(this.rbtnMasculino);
            this.gbSexo.Controls.Add(this.rbtnFeminino);
            this.gbSexo.Location = new System.Drawing.Point(132, 6);
            this.gbSexo.Name = "gbSexo";
            this.gbSexo.Size = new System.Drawing.Size(121, 90);
            this.gbSexo.TabIndex = 12;
            this.gbSexo.TabStop = false;
            this.gbSexo.Text = "Sexo:";
            // 
            // rbtnMasculino
            // 
            this.rbtnMasculino.AutoSize = true;
            this.rbtnMasculino.Checked = true;
            this.rbtnMasculino.Location = new System.Drawing.Point(42, 24);
            this.rbtnMasculino.Name = "rbtnMasculino";
            this.rbtnMasculino.Size = new System.Drawing.Size(73, 17);
            this.rbtnMasculino.TabIndex = 1;
            this.rbtnMasculino.TabStop = true;
            this.rbtnMasculino.Text = "Masculino";
            this.rbtnMasculino.UseVisualStyleBackColor = true;
            // 
            // rbtnFeminino
            // 
            this.rbtnFeminino.AutoSize = true;
            this.rbtnFeminino.Location = new System.Drawing.Point(42, 47);
            this.rbtnFeminino.Name = "rbtnFeminino";
            this.rbtnFeminino.Size = new System.Drawing.Size(67, 17);
            this.rbtnFeminino.TabIndex = 2;
            this.rbtnFeminino.TabStop = true;
            this.rbtnFeminino.Text = "Feminino";
            this.rbtnFeminino.UseVisualStyleBackColor = true;
            // 
            // lblOutrosDados
            // 
            this.lblOutrosDados.AutoSize = true;
            this.lblOutrosDados.Location = new System.Drawing.Point(-1, -1);
            this.lblOutrosDados.Name = "lblOutrosDados";
            this.lblOutrosDados.Size = new System.Drawing.Size(127, 13);
            this.lblOutrosDados.TabIndex = 128;
            this.lblOutrosDados.Text = "Outros Dados Cadastrais:";
            // 
            // pnlIdentificacao
            // 
            this.pnlIdentificacao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlIdentificacao.Controls.Add(this.txtId);
            this.pnlIdentificacao.Controls.Add(this.lblId);
            this.pnlIdentificacao.Controls.Add(this.txtNacionalidade);
            this.pnlIdentificacao.Controls.Add(this.lblNacionalidade);
            this.pnlIdentificacao.Controls.Add(this.lblIdentifica);
            this.pnlIdentificacao.Controls.Add(this.imgPerfil);
            this.pnlIdentificacao.Controls.Add(this.txtFuncao);
            this.pnlIdentificacao.Controls.Add(this.txtEstadoCivil);
            this.pnlIdentificacao.Controls.Add(this.txtNatural);
            this.pnlIdentificacao.Controls.Add(this.lblFuncao);
            this.pnlIdentificacao.Controls.Add(this.lblEstadoCivil);
            this.pnlIdentificacao.Controls.Add(this.lblNatural);
            this.pnlIdentificacao.Controls.Add(this.mkdDataBatismo);
            this.pnlIdentificacao.Controls.Add(this.lblDataBatismo);
            this.pnlIdentificacao.Controls.Add(this.mkdCpf);
            this.pnlIdentificacao.Controls.Add(this.lblCpf);
            this.pnlIdentificacao.Controls.Add(this.mkdRg);
            this.pnlIdentificacao.Controls.Add(this.lblRg);
            this.pnlIdentificacao.Controls.Add(this.txtNomeMae);
            this.pnlIdentificacao.Controls.Add(this.txtNomePai);
            this.pnlIdentificacao.Controls.Add(this.lblNomeMae);
            this.pnlIdentificacao.Controls.Add(this.lblNomePai);
            this.pnlIdentificacao.Controls.Add(this.mkdDataNascimento);
            this.pnlIdentificacao.Controls.Add(this.lblDataNascimento);
            this.pnlIdentificacao.Controls.Add(this.txtNomeCompleto);
            this.pnlIdentificacao.Controls.Add(this.lblNome);
            this.pnlIdentificacao.Location = new System.Drawing.Point(12, 3);
            this.pnlIdentificacao.Name = "pnlIdentificacao";
            this.pnlIdentificacao.Size = new System.Drawing.Size(738, 153);
            this.pnlIdentificacao.TabIndex = 1;
            // 
            // txtId
            // 
            this.txtId.Enabled = false;
            this.txtId.Location = new System.Drawing.Point(599, 21);
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(33, 20);
            this.txtId.TabIndex = 142;
            // 
            // lblId
            // 
            this.lblId.AutoSize = true;
            this.lblId.Location = new System.Drawing.Point(577, 24);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(16, 13);
            this.lblId.TabIndex = 141;
            this.lblId.Text = "Id";
            // 
            // txtNacionalidade
            // 
            this.txtNacionalidade.Location = new System.Drawing.Point(120, 73);
            this.txtNacionalidade.Name = "txtNacionalidade";
            this.txtNacionalidade.Size = new System.Drawing.Size(220, 20);
            this.txtNacionalidade.TabIndex = 7;
            this.txtNacionalidade.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNacionalidade_KeyPress);
            this.txtNacionalidade.Leave += new System.EventHandler(this.txtNacionalidade_Leave);
            // 
            // lblNacionalidade
            // 
            this.lblNacionalidade.AutoSize = true;
            this.lblNacionalidade.Location = new System.Drawing.Point(39, 76);
            this.lblNacionalidade.Name = "lblNacionalidade";
            this.lblNacionalidade.Size = new System.Drawing.Size(75, 13);
            this.lblNacionalidade.TabIndex = 140;
            this.lblNacionalidade.Text = "Nacionalidade";
            // 
            // lblIdentifica
            // 
            this.lblIdentifica.AutoSize = true;
            this.lblIdentifica.Location = new System.Drawing.Point(-1, 0);
            this.lblIdentifica.Name = "lblIdentifica";
            this.lblIdentifica.Size = new System.Drawing.Size(71, 13);
            this.lblIdentifica.TabIndex = 139;
            this.lblIdentifica.Text = "Identificação:";
            // 
            // imgPerfil
            // 
            this.imgPerfil.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.imgPerfil.Image = ((System.Drawing.Image)(resources.GetObject("imgPerfil.Image")));
            this.imgPerfil.InitialImage = null;
            this.imgPerfil.Location = new System.Drawing.Point(638, 21);
            this.imgPerfil.Name = "imgPerfil";
            this.imgPerfil.Size = new System.Drawing.Size(95, 117);
            this.imgPerfil.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgPerfil.TabIndex = 138;
            this.imgPerfil.TabStop = false;
            this.imgPerfil.DoubleClick += new System.EventHandler(this.imgPerfil_DoubleClick);
            // 
            // txtFuncao
            // 
            this.txtFuncao.Location = new System.Drawing.Point(515, 125);
            this.txtFuncao.Name = "txtFuncao";
            this.txtFuncao.Size = new System.Drawing.Size(117, 20);
            this.txtFuncao.TabIndex = 12;
            this.txtFuncao.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFuncao_KeyPress);
            this.txtFuncao.Leave += new System.EventHandler(this.txtFuncao_Leave);
            // 
            // txtEstadoCivil
            // 
            this.txtEstadoCivil.Location = new System.Drawing.Point(515, 99);
            this.txtEstadoCivil.Name = "txtEstadoCivil";
            this.txtEstadoCivil.Size = new System.Drawing.Size(117, 20);
            this.txtEstadoCivil.TabIndex = 11;
            this.txtEstadoCivil.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEstadoCivil_KeyPress);
            this.txtEstadoCivil.Leave += new System.EventHandler(this.txtEstadoCivil_Leave);
            // 
            // txtNatural
            // 
            this.txtNatural.Location = new System.Drawing.Point(482, 73);
            this.txtNatural.Name = "txtNatural";
            this.txtNatural.Size = new System.Drawing.Size(150, 20);
            this.txtNatural.TabIndex = 8;
            this.txtNatural.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNatural_KeyPress);
            this.txtNatural.Leave += new System.EventHandler(this.txtNatural_Leave);
            // 
            // lblFuncao
            // 
            this.lblFuncao.AutoSize = true;
            this.lblFuncao.Location = new System.Drawing.Point(466, 128);
            this.lblFuncao.Name = "lblFuncao";
            this.lblFuncao.Size = new System.Drawing.Size(43, 13);
            this.lblFuncao.TabIndex = 134;
            this.lblFuncao.Text = "Função";
            // 
            // lblEstadoCivil
            // 
            this.lblEstadoCivil.AutoSize = true;
            this.lblEstadoCivil.Location = new System.Drawing.Point(447, 102);
            this.lblEstadoCivil.Name = "lblEstadoCivil";
            this.lblEstadoCivil.Size = new System.Drawing.Size(62, 13);
            this.lblEstadoCivil.TabIndex = 133;
            this.lblEstadoCivil.Text = "Estado Civil";
            // 
            // lblNatural
            // 
            this.lblNatural.AutoSize = true;
            this.lblNatural.Location = new System.Drawing.Point(409, 76);
            this.lblNatural.Name = "lblNatural";
            this.lblNatural.Size = new System.Drawing.Size(67, 13);
            this.lblNatural.TabIndex = 132;
            this.lblNatural.Text = "Naturalidade";
            // 
            // mkdDataBatismo
            // 
            this.mkdDataBatismo.Location = new System.Drawing.Point(267, 47);
            this.mkdDataBatismo.Mask = "00/00/0000";
            this.mkdDataBatismo.Name = "mkdDataBatismo";
            this.mkdDataBatismo.Size = new System.Drawing.Size(73, 20);
            this.mkdDataBatismo.TabIndex = 4;
            this.mkdDataBatismo.ValidatingType = typeof(System.DateTime);
            // 
            // lblDataBatismo
            // 
            this.lblDataBatismo.AutoSize = true;
            this.lblDataBatismo.Location = new System.Drawing.Point(217, 50);
            this.lblDataBatismo.Name = "lblDataBatismo";
            this.lblDataBatismo.Size = new System.Drawing.Size(44, 13);
            this.lblDataBatismo.TabIndex = 130;
            this.lblDataBatismo.Text = "Batismo";
            // 
            // mkdCpf
            // 
            this.mkdCpf.Location = new System.Drawing.Point(535, 47);
            this.mkdCpf.Mask = "000.000.000-00";
            this.mkdCpf.Name = "mkdCpf";
            this.mkdCpf.Size = new System.Drawing.Size(97, 20);
            this.mkdCpf.TabIndex = 6;
            // 
            // lblCpf
            // 
            this.lblCpf.AutoSize = true;
            this.lblCpf.Location = new System.Drawing.Point(502, 50);
            this.lblCpf.Name = "lblCpf";
            this.lblCpf.Size = new System.Drawing.Size(27, 13);
            this.lblCpf.TabIndex = 128;
            this.lblCpf.Text = "CPF";
            // 
            // mkdRg
            // 
            this.mkdRg.Location = new System.Drawing.Point(412, 47);
            this.mkdRg.Mask = "00.000.000/0";
            this.mkdRg.Name = "mkdRg";
            this.mkdRg.Size = new System.Drawing.Size(82, 20);
            this.mkdRg.TabIndex = 5;
            // 
            // lblRg
            // 
            this.lblRg.AutoSize = true;
            this.lblRg.Location = new System.Drawing.Point(383, 50);
            this.lblRg.Name = "lblRg";
            this.lblRg.Size = new System.Drawing.Size(23, 13);
            this.lblRg.TabIndex = 126;
            this.lblRg.Text = "RG";
            // 
            // txtNomeMae
            // 
            this.txtNomeMae.Location = new System.Drawing.Point(120, 125);
            this.txtNomeMae.Name = "txtNomeMae";
            this.txtNomeMae.Size = new System.Drawing.Size(266, 20);
            this.txtNomeMae.TabIndex = 10;
            this.txtNomeMae.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNomeMae_KeyPress);
            this.txtNomeMae.Leave += new System.EventHandler(this.txtNomeMae_Leave);
            // 
            // txtNomePai
            // 
            this.txtNomePai.Location = new System.Drawing.Point(120, 99);
            this.txtNomePai.Name = "txtNomePai";
            this.txtNomePai.Size = new System.Drawing.Size(266, 20);
            this.txtNomePai.TabIndex = 9;
            this.txtNomePai.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNomePai_KeyPress);
            this.txtNomePai.Leave += new System.EventHandler(this.txtNomePai_Leave);
            // 
            // lblNomeMae
            // 
            this.lblNomeMae.AutoSize = true;
            this.lblNomeMae.Location = new System.Drawing.Point(38, 128);
            this.lblNomeMae.Name = "lblNomeMae";
            this.lblNomeMae.Size = new System.Drawing.Size(76, 13);
            this.lblNomeMae.TabIndex = 123;
            this.lblNomeMae.Text = "Nome Da Mãe";
            // 
            // lblNomePai
            // 
            this.lblNomePai.AutoSize = true;
            this.lblNomePai.Location = new System.Drawing.Point(43, 102);
            this.lblNomePai.Name = "lblNomePai";
            this.lblNomePai.Size = new System.Drawing.Size(70, 13);
            this.lblNomePai.TabIndex = 122;
            this.lblNomePai.Text = "Nome Do Pai";
            // 
            // mkdDataNascimento
            // 
            this.mkdDataNascimento.Location = new System.Drawing.Point(120, 47);
            this.mkdDataNascimento.Mask = "00/00/0000";
            this.mkdDataNascimento.Name = "mkdDataNascimento";
            this.mkdDataNascimento.Size = new System.Drawing.Size(73, 20);
            this.mkdDataNascimento.TabIndex = 3;
            this.mkdDataNascimento.ValidatingType = typeof(System.DateTime);
            // 
            // lblDataNascimento
            // 
            this.lblDataNascimento.AutoSize = true;
            this.lblDataNascimento.Location = new System.Drawing.Point(51, 50);
            this.lblDataNascimento.Name = "lblDataNascimento";
            this.lblDataNascimento.Size = new System.Drawing.Size(63, 13);
            this.lblDataNascimento.TabIndex = 120;
            this.lblDataNascimento.Text = "Nascimento";
            // 
            // txtNomeCompleto
            // 
            this.txtNomeCompleto.Location = new System.Drawing.Point(120, 21);
            this.txtNomeCompleto.Name = "txtNomeCompleto";
            this.txtNomeCompleto.Size = new System.Drawing.Size(451, 20);
            this.txtNomeCompleto.TabIndex = 1;
            this.txtNomeCompleto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNomeCompleto_KeyPress);
            // 
            // lblNome
            // 
            this.lblNome.AutoSize = true;
            this.lblNome.Location = new System.Drawing.Point(32, 24);
            this.lblNome.Name = "lblNome";
            this.lblNome.Size = new System.Drawing.Size(82, 13);
            this.lblNome.TabIndex = 116;
            this.lblNome.Text = "Nome Completo";
            // 
            // frmCadastro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CausesValidation = false;
            this.ClientSize = new System.Drawing.Size(758, 501);
            this.ControlBox = false;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pnlMenuBaixo);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(300, 141);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCadastro";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Cadastro";
            this.Load += new System.EventHandler(this.formularioCad_Load);
            this.pnlMenuBaixo.ResumeLayout(false);
            this.pnlLocalizacao.ResumeLayout(false);
            this.pnlLocalizacao.PerformLayout();
            this.pnlContatos.ResumeLayout(false);
            this.pnlContatos.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.pnlOutrosDados.ResumeLayout(false);
            this.pnlOutrosDados.PerformLayout();
            this.gbStatus.ResumeLayout(false);
            this.gbStatus.PerformLayout();
            this.gbBatizado.ResumeLayout(false);
            this.gbBatizado.PerformLayout();
            this.gbRecebidoPor.ResumeLayout(false);
            this.gbRecebidoPor.PerformLayout();
            this.gbSexo.ResumeLayout(false);
            this.gbSexo.PerformLayout();
            this.pnlIdentificacao.ResumeLayout(false);
            this.pnlIdentificacao.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgPerfil)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlMenuBaixo;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnImprimir;
        private System.Windows.Forms.Button btnSalvarAtualizar;
        private System.Windows.Forms.Panel pnlLocalizacao;
        private System.Windows.Forms.Label lblLocaliza;
        private System.Windows.Forms.TextBox txtBairro;
        private System.Windows.Forms.Label lblBairro;
        private System.Windows.Forms.TextBox txtCidade;
        private System.Windows.Forms.Label lblCidade;
        private System.Windows.Forms.TextBox txtUf;
        private System.Windows.Forms.Label lblUf;
        private System.Windows.Forms.TextBox txtComplemento;
        private System.Windows.Forms.Label lblComplemento;
        private System.Windows.Forms.TextBox txtNumero;
        private System.Windows.Forms.Label lblNumero;
        private System.Windows.Forms.TextBox txtEndereco;
        private System.Windows.Forms.Label lblEndereco;
        private System.Windows.Forms.MaskedTextBox mkdCep;
        private System.Windows.Forms.Label lblCep;
        private System.Windows.Forms.Panel pnlContatos;
        private System.Windows.Forms.Label lblContatos;
        private System.Windows.Forms.TextBox txtMail;
        private System.Windows.Forms.Label lblMail;
        private System.Windows.Forms.MaskedTextBox mkdCelular;
        private System.Windows.Forms.Label lblCelular;
        private System.Windows.Forms.MaskedTextBox mkdTelefone;
        private System.Windows.Forms.Label lblTelefone;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pnlOutrosDados;
        private System.Windows.Forms.GroupBox gbStatus;
        private System.Windows.Forms.RadioButton rbtnInativo;
        private System.Windows.Forms.RadioButton rbtnAtivo;
        private System.Windows.Forms.GroupBox gbBatizado;
        private System.Windows.Forms.RadioButton rbtnNao;
        private System.Windows.Forms.RadioButton rbtnSim;
        private System.Windows.Forms.GroupBox gbRecebidoPor;
        private System.Windows.Forms.RadioButton rbtnTransferencia;
        private System.Windows.Forms.RadioButton rbtnAdesao;
        private System.Windows.Forms.RadioButton rbtnBatismo;
        private System.Windows.Forms.GroupBox gbSexo;
        private System.Windows.Forms.RadioButton rbtnMasculino;
        private System.Windows.Forms.RadioButton rbtnFeminino;
        private System.Windows.Forms.Label lblOutrosDados;
        private System.Windows.Forms.Panel pnlIdentificacao;
        private System.Windows.Forms.TextBox txtId;
        private System.Windows.Forms.Label lblId;
        private System.Windows.Forms.TextBox txtNacionalidade;
        private System.Windows.Forms.Label lblNacionalidade;
        private System.Windows.Forms.Label lblIdentifica;
        private System.Windows.Forms.PictureBox imgPerfil;
        private System.Windows.Forms.TextBox txtFuncao;
        private System.Windows.Forms.TextBox txtEstadoCivil;
        private System.Windows.Forms.TextBox txtNatural;
        private System.Windows.Forms.Label lblFuncao;
        private System.Windows.Forms.Label lblEstadoCivil;
        private System.Windows.Forms.Label lblNatural;
        private System.Windows.Forms.MaskedTextBox mkdDataBatismo;
        private System.Windows.Forms.Label lblDataBatismo;
        private System.Windows.Forms.MaskedTextBox mkdCpf;
        private System.Windows.Forms.Label lblCpf;
        private System.Windows.Forms.MaskedTextBox mkdRg;
        private System.Windows.Forms.Label lblRg;
        private System.Windows.Forms.TextBox txtNomeMae;
        private System.Windows.Forms.TextBox txtNomePai;
        private System.Windows.Forms.Label lblNomeMae;
        private System.Windows.Forms.Label lblNomePai;
        private System.Windows.Forms.MaskedTextBox mkdDataNascimento;
        private System.Windows.Forms.Label lblDataNascimento;
        private System.Windows.Forms.TextBox txtNomeCompleto;
        private System.Windows.Forms.Label lblNome;
    }
}