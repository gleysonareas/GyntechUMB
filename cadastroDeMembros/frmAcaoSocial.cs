﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;

namespace GyntechUMB
{
    public partial class frmAcaoSocial : Form
    {
        #region Programa Principal
        public frmAcaoSocial()
        {
            InitializeComponent();
        }
        private void frmAcaoSocial_Activated(object sender, EventArgs e)
        {
            carregarAcao();
            //verificarTempoExtq();
        }

        #region Logica das Caixas de Texto
        //Validação da combo box entrada
        private void cbTpEnt_Click(object sender, EventArgs e)
        {
            if (cbTpEnt.SelectedIndex == -1)
                cbTpEnt.ValueMember = "none";
        }

        //Validação da combo box saída
        private void cbTpSai_Click(object sender, EventArgs e)
        {
            if (cbTpSai.SelectedIndex == -1)
                cbTpSai.ValueMember = "none";
        }

        //Validação do campo nome do produto
        private void txtNomeProduto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsLetter(e.KeyChar)) && (e.KeyChar != (char)8) && (e.KeyChar != (char)32))
                e.Handled = true;
        }

        //Quantidade
        private void txtQtd_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((char.IsLetter(e.KeyChar)) && (e.KeyChar != (char)8))
                e.Handled = true;
        }

        //Descrição
        private void txtDescricao_Leave(object sender, EventArgs e)
        {
            if (txtDescricao.Text == "")
            {
                txtDescricao.ForeColor = Color.LightGray;
                txtDescricao.Text = "O campo DESCRIÇÃO necessita ser preenchido";
            }
        }
        #endregion

        #region Logica dos Botões
        //Tipo de lançamento
        private void rbtnEntrada_CheckedChanged(object sender, EventArgs e)
        {
            trocarTipo();
        }

        private void rbtnSaida_CheckedChanged(object sender, EventArgs e)
        {
            trocarTipo();
        }

        //Botão lançar
        private void btnLancar_Click(object sender, EventArgs e)
        {
            List<string> mensagem = ValidarTela();

            if (mensagem.Count == 0)
            {
                salvarNoBanco();
                carregarAcao();
                limparCampo();
            }
            else
            {
                string msg = "";
                foreach (var m in mensagem)
                {
                    msg = msg + Environment.NewLine + m;
                }
                MessageBox.Show(msg, "Erros no preenchimento", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        //Botão Sair
        private void btnSair_Click(object sender, EventArgs e)
        {
            fecharJanela();
        }

        #endregion

        #endregion

        #region Funções
        //Função de validação das caixas de texto
        private List<string> ValidarTela()
        {
            List<string> erros = new List<string>();

            //Validação da Campo Entrada
            if ((string.IsNullOrWhiteSpace(cbTpEnt.Text)) && (rbtnEntrada.Checked == true) && (rbtnSaida.Checked == false))
                erros.Add("Tipo de Entrada não selecionado!");

            //Validação do campo Saída
            if ((string.IsNullOrWhiteSpace(cbTpSai.Text)) && (rbtnEntrada.Checked == false) && (rbtnSaida.Checked == true))
                erros.Add("Tipo de Saída não selecionado!");

            //Validação do Nome do Produto
            if (string.IsNullOrWhiteSpace(txtNomeProduto.Text))
                erros.Add("Nome do produto não digitado!");

            // Validação da Quantidade
            if (string.IsNullOrWhiteSpace(txtQtd.Text))
                erros.Add("Quantidade não especificada!");
            else if ((int.Parse(txtQtd.Text) <= 0) && (rbtnEntrada.Checked == false) && (rbtnSaida.Checked == true))
                erros.Add("Alimento com estoque zerado!");

            //Validação da Validade
            DateTime dataVal;
            if(DateTime.TryParse(mkdValidade.Text, out dataVal))
            {
                if (dataVal.Year < 1900 || dataVal.Year > DateTime.Today.Year)
                {
                    erros.Add("Data de validade invalidada!");
                }
            }
            //Validação da Data de Entrada
            DateTime dataEnt;
            if(DateTime.TryParse(mkdDtEnt.Text, out dataEnt))
            {
                if(dataEnt.Year < 1900 || dataEnt.Year > DateTime.Today.Year && rbtnEntrada.Checked == true)
                {
                    erros.Add("Insira uma data valida!");
                }
            }
            else
            {
                erros.Add("Insira uma data valida!");
            }
            //Validação da Data de Saída
                DateTime dataSai;
                if (DateTime.TryParse(mkdDtSai.Text, out dataSai))
                {
                    if (dataSai.Year < dataEnt.Year || dataSai.Year > DateTime.Today.Year && rbtnSaida.Checked == true)
                    {
                        erros.Add("Data de saída inválida!");
                    }
                }
                else
                {
                    if (rbtnEntrada.Checked == false)
                        erros.Add("Data de saída inválida!");
                }
            
            //Validação da Descrição
            if (string.IsNullOrWhiteSpace(txtDescricao.Text))
                erros.Add("Descrição inválida!");

            return erros;
        }

        //Função esconde campo
        private bool trocarTipo()
        {
            if (rbtnEntrada.Checked == true)
            {
                lblTpEntrada.Visible = true;
                cbTpEnt.Visible = true;
                lblTpSaida.Visible = false;
                cbTpSai.Visible = false;
                cbTpSai.ResetText();
                return true;
            }
            else
            {
                lblTpEntrada.Visible = false;
                cbTpEnt.Visible = false;
                lblTpSaida.Visible = true;
                cbTpSai.Visible = true;
                cbTpEnt.ResetText();
                return false;
            }
        }

        //Função que realiza a verificação de alimentos parados a mais  de 30 dias em extoque
        private void verificarTempoExtq()
        {
            throw new NotImplementedException();
        }

        //Função Limpa Campo
        private void limparCampo()
        {
            cbTpEnt.ResetText();
            cbTpSai.ResetText();
            txtNomeProduto.Clear();
            txtQtd.Clear();
            txtFabricante.Clear();
            mkdValidade.Clear();
            mkdDtEnt.Clear();
            mkdDtSai.Clear();
            txtDescricao.Clear();
        }

        //Função que volta para FORM anterior
        private void fecharJanela()
        {
            this.Close();
        }

        #endregion

        #region Dados
        //Função que realiza o INSERT no banco
        private void salvarNoBanco()
        {
            string inserir = "insert into acaoSocial(lancamento, entrada, saida, nomeProduto, quantidade, fabricante, validade, dataEntrada, dataSaida, descricao)" +
                 "values(@lancamento, @entrada, @saida, @nomeProduto, @quantidade, @fabricante, @validade, @dataEntrada, @dataSaida, @descricao)";

            operarAlteracoesNoBanco(inserir);
        }

        //Função que realiza o SELECT no banco
        private void carregarAcao()
        {
            string connectionstring = "Data Source=localhost;Initial Catalog=teste;Integrated Security=True;Connect Timeout=30";
            using (SqlConnection con = new SqlConnection(connectionstring))
            {
                con.Open();

                SqlCommand c = new SqlCommand("select id, lancamento, entrada, saida, nomeProduto, quantidade, fabricante, validade, dataEntrada, dataSaida, descricao from acaoSocial", con);

                SqlDataAdapter d = new SqlDataAdapter(c);
                DataTable t = new DataTable();
                d.Fill(t);
                gdAcSoc.DataSource = t;

                con.Close();
            }
        }

        //Função que realiza operações no BANCO
        private void operarAlteracoesNoBanco(string sqlParametro)
        {
            string connectionstring = "Data Source = localhost; Initial Catalog = teste; Integrated Security = True; Connect Timeout = 30";
            using (SqlConnection con = new SqlConnection(connectionstring))
            {
                string sql = sqlParametro;
                SqlCommand c = new SqlCommand(sql, con);

                //lançamento
                if (rbtnEntrada.Checked == true)
                    c.Parameters.AddWithValue("@lancamento", "Entrada");
                else
                    c.Parameters.AddWithValue("@lancamento", "Saída");

                //Tipo de entrada
                if (cbTpEnt.Text != "")
                    c.Parameters.AddWithValue("@entrada", cbTpEnt.Text);
                else
                    c.Parameters.AddWithValue("@entrada", DBNull.Value);

                //Tipo de saída
                if (cbTpSai.Text != "")
                    c.Parameters.AddWithValue("@saida", cbTpSai.Text);
                else
                    c.Parameters.AddWithValue("@saida", DBNull.Value);

                //Nome do produto
                c.Parameters.AddWithValue("@nomeProduto", txtNomeProduto.Text);

                //Quantidade
                c.Parameters.AddWithValue("@quantidade", txtQtd.Text);

                //Fabricante
                if (txtFabricante.Text != "")
                    c.Parameters.AddWithValue("@fabricante", txtFabricante.Text);
                else if (cbTpEnt.Text != "Roupas" || cbTpEnt.Text != "Outros")
                    c.Parameters.AddWithValue("@fabricante", DBNull.Value);

                //Validade
                mkdValidade.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;
                if (mkdValidade.Text != "")
                    c.Parameters.AddWithValue("@validade", Convert.ToDateTime(mkdValidade.Text));
                else if(cbTpEnt.Text != "Roupas" || cbTpEnt.Text != "Outros")
                    c.Parameters.AddWithValue("@validade", DBNull.Value);
                mkdValidade.TextMaskFormat = MaskFormat.IncludePromptAndLiterals;

                //Data de entrada
                if (mkdDtEnt.Text != "" && rbtnEntrada.Checked == true)
                    c.Parameters.AddWithValue("@dataEntrada", Convert.ToDateTime(mkdDtEnt.Text));

                //Data de saída
                if (mkdDtSai.Text != "" && mkdDtEnt.Text != "" && rbtnSaida.Checked == true)
                    c.Parameters.AddWithValue("@dataSaida", Convert.ToDateTime(mkdDtSai.Text));
                else
                    c.Parameters.AddWithValue("@dataSaida", DBNull.Value);

                //Descrição
                c.Parameters.AddWithValue("@descricao", txtDescricao.Text);

                con.Open();
                c.ExecuteNonQuery();
                con.Close();
            }
        }

        //Função editar campos
        private void editarCampo()
        {

        }

        #endregion
    }
}
