﻿using System.Collections.Generic;
using Gyntech.DTO;
using Gyntech.DAL;
using System;

namespace Gyntech.Model
{
    public class MembroModel
    {
        public int Inserir(MembroDTO mDto)
        {
            return new MembroDAL().Inserir(mDto);
        }

        public IList<MembroDTO> CarregarMembros()
        {
            return new MembroDAL().CarregarMembros();
        }

        public int Atulizar(MembroDTO mDto)
        {
            return new MembroDAL().Atulizar(mDto);
        }

        public IList<MembroDTO> CarregarDoBanco(int idMembroSelecionado)
        {
            return new MembroDAL().CarregarDoBanco(idMembroSelecionado);
        }

        public int AlterarStatus(MembroDTO mDto)
        {
            return new MembroDAL().AlterarStatus(mDto);
        }
    }
}
