﻿using Gyntech.DTO;
using Gyntech.DAL;
using System;
using System.Collections.Generic;

namespace Gyntech.Model
{
    public class FinanceiroModel
    {
        public int Inserir(FinanceiroDTO fDTO)
        {
            return new FinanceiroDAL().Inserir(fDTO);
        }

        public IList<FinanceiroDTO> CarregarFinancas()
        {
            return new FinanceiroDAL().CarregarFinancas();
        }

        public IList<FinanceiroDTO> CarregarDoBanco(int idSelecionado)
        {
            return new FinanceiroDAL().CarregarDoBanco(idSelecionado);
        }

        public int Atualizar(FinanceiroDTO fDTO)
        {
            return new FinanceiroDAL().Atualizar(fDTO);
        }
    }
}
